import {combineReducers} from 'redux';

import accountReducer from '../modules/Account/reducer';
import navbarReducer from '../modules/Navbar/reducer';
import walletsReducer from '../modules/WalletsList/reducer';
import chartsReducer from '../modules/Charts/reducer';

import {IMPORT_BACKUP} from '../modules/Account/constants';

const appReducers = combineReducers({
  navbar: navbarReducer,
  charts: chartsReducer,
  wallets: walletsReducer,
  account: accountReducer,
});

export default (state, action) => {
  if (action.type === IMPORT_BACKUP) {
    return action.payload;
  }
  return appReducers(state, action);
};
