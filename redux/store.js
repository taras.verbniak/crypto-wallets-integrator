import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer, createTransform} from 'redux-persist';

import reducers from './reducers';

import {loadBalancesAction} from '../modules/WalletsList/actions';

// We need this custom validator just because nested blacklist/whitlist doesn't work
const BlacklistTransform = createTransform(
  null,
  (outboundState) => {
    outboundState.checkWalletError = null;
    outboundState.isAddWalletModalOpened = false;
    outboundState.isWalletChecking = false;
    outboundState.walletForEditing = null;

    return outboundState;
  },
  {whitelist: ['wallets']},
);

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navbar'],
  transforms: [BlacklistTransform],
};

const persistedReducer = persistReducer(persistConfig, reducers);
const logger = createLogger();

const store = createStore(persistedReducer, applyMiddleware(logger, thunk));

const persistor = persistStore(store, null, () => {
  loadBalancesAction()(store.dispatch, store.getState);
});

export {store, persistor};
