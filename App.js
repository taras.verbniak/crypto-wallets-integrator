import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {store, persistor} from './redux/store';

import {initLocalization} from './localization';

import Loader from './modules/Loader';
import Root from './modules/Root';

// Init locale in onBeforeLift, since we need to wait for saved locale from redux store
const App = () => (
  <Provider store={store}>
    <PersistGate
      loading={<Loader />}
      persistor={persistor}
      onBeforeLift={() => initLocalization(store)}>
      <Root />
    </PersistGate>
  </Provider>
);

export default App;
