# Start

- install **react-native-cli** by running `sudo npm i -g react-native-cli`
- install **Xcode** (for iOS) and **Android Studio** (for Android) to be able to test locally
- clone project, run `yarn` to install dependecies
- run `react-native eject` to create **/ios** and **/android** folders with their dependencies (see community [react-native-eject](https://github.com/ramyareye/react-native-eject) library, since native lib is not working with the last react-native versions)
- run `react-native link react-native-vector-icons` to link icons lib
- go to **/ios** folder and run `pod install` to install iOS dependencies
- open **/ios/cwi.xcodeproj/project.pbxproj** search in this file for **/\* EvilIcons.ttf**. The last 2 results will be in blocks of fonts. One block with `children = (` another with `files = (` wrapper. It's a duplication. You should deleted last block including identifier (hash) and `/* Resources */ = {` and up to the end of fonts.
- run `react-native run-ios` to start the application, it should be opened in virtual device

# Styling

| Type             | Color                                                           |
| ---------------- | --------------------------------------------------------------- |
| Modal background | `#141f2e`                                                       |
| Modal overlay    | `#fff` + `opacity: 0.7` or equivalent - `rgba(255,255,255,0.7)` |
| Btn primary      | bg `#2488DC` + text `#fff`                                      |
| Btn error        | bg `red` + text `#fff`                                          |
| Btn disabled     | bg `#c5c5c5` + text `#fff`                                      |
