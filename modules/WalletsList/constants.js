export const moduleName = 'wallets';

export const COINS = {
  BTC: {
    position: 1,
    symbol: 'BTC',
    title: 'Bitcoin (BTC)',
    iconSource: require('../../icons/btc.png'),
  },
  ETH: {
    position: 2,
    symbol: 'ETH',
    title: 'Ethereum (ETH)',
    iconSource: require('../../icons/eth.png'),
  },
  XRP: {
    position: 3,
    symbol: 'XRP',
    title: 'Ripple (XRP)',
    iconSource: require('../../icons/xrp.png'),
  },
  USDT: {
    position: 4,
    symbol: 'USDT',
    title: 'Tether (USDT)',
    iconSource: require('../../icons/usdt.png'),
  },
  USDC: {
    position: 5,
    symbol: 'USDC',
    title: 'USD Coin (USDC)',
    iconSource: require('../../icons/usdc.png'),
  },
};

export const COINS_LIST = Object.values(COINS);

export const TYPE_LIST = [
  {
    title: 'Public address',
    iconName: 'wallet',
  },
  {
    title: 'Amount',
    iconName: 'database',
  },
];

export const OPEN_ADD_WALLET_MODAL = `${moduleName}/OPEN_ADD_WALLET_MODAL`;
export const CLOSE_ADD_WALLET_MODAL = `${moduleName}/CLOSE_ADD_WALLET_MODAL`;
export const ADD_WALLET_START = `${moduleName}/ADD_WALLET_START`;
export const ADD_WALLET_FAIL = `${moduleName}/ADD_WALLET_FAIL`;
export const CHANGE_WALLETS = `${moduleName}/CHANGE_WALLETS`;
export const LOAD_BALANCES_START = `${moduleName}/LOAD_BALANCES_START`;
export const LOAD_BALANCES_SUCCESS = `${moduleName}/LOAD_BALANCES_SUCCESS`;
export const LOAD_BALANCES_FAIL = `${moduleName}/LOAD_BALANCES_FAIL`;
export const SAVE_RATES = `${moduleName}/SAVE_RATES`;
