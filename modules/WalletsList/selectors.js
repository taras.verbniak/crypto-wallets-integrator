import {createSelector} from 'reselect';
import {moduleName} from './constants';

export const getWallets = (state) => state[moduleName].wallets;
export const getGroupedWallets = createSelector(getWallets, (wallets) =>
  wallets
    ?.reduce((acc, item) => {
      const {symbol} = item;
      let blockIndex = acc.findIndex(
        (accBlock) => accBlock[0].symbol === symbol,
      );
      if (blockIndex === -1) {
        blockIndex = acc.length;
        acc[blockIndex] = [];
      }

      acc[blockIndex] = [...acc[blockIndex], item];
      return acc;
    }, [])
    .sort((_a, _b) => {
      const a = _a[0].title;
      const b = _b[0].title;

      return a > b ? 1 : a < b ? -1 : 0;
    }),
);

export const getTotal = createSelector(getWallets, (wallets) =>
  wallets.reduce((acc, w) => (acc += w.balanceFiat), 0),
);

export const getIsAddWalletModalOpened = (state) =>
  state[moduleName].isAddWalletModalOpened;
export const getIsWalletChecking = (state) =>
  state[moduleName].isWalletChecking;
export const getCheckWalletError = (state) =>
  state[moduleName].checkWalletError;
export const getIsBalancesLoading = (state) =>
  state[moduleName].isBalancesLoading;
export const getWalletForEditing = (state) =>
  state[moduleName].walletForEditing;
export const getRatesValidityDate = (state) =>
  state[moduleName].ratesValidityDate;
export const getRates = (state) => state[moduleName].rates;
