import React from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import AddWalletModal from './components/AddWalletModal';
import Wallets from './components/Wallets';

import {
  getDisplayMode,
  getCurrencySelector,
  getRoundWalletsSelector,
} from '../Account/selectors';

import {
  addWalletAction,
  deleteWalletAction,
  updateWalletAction,
  openAddWalletModalAction,
  closeAddWalletModalAction,
} from './actions';
import {
  getGroupedWallets,
  getCheckWalletError,
  getIsWalletChecking,
  getWalletForEditing,
  getIsBalancesLoading,
  getIsAddWalletModalOpened,
} from './selectors';

const WalletsList = ({
  currency,
  addWallet,
  displayMode,
  updateWallet,
  deleteWallet,
  roundWallets,
  groupedWallets,
  checkWalletError,
  isWalletChecking,
  walletForEditing,
  isBalancesLoading,
  openAddWalletModal,
  closeAddWalletModal,
  isAddWalletModalOpened,
}) => (
  <View style={styles.walletsList}>
    <Wallets
      currency={currency}
      wallets={groupedWallets}
      displayMode={displayMode}
      roundWallets={roundWallets}
      isBalancesLoading={isBalancesLoading}
      openAddWalletModal={openAddWalletModal}
    />
    <AddWalletModal
      addWallet={addWallet}
      updateWallet={updateWallet}
      deleteWallet={deleteWallet}
      close={closeAddWalletModal}
      show={isAddWalletModalOpened}
      walletForEditing={walletForEditing}
      isWalletChecking={isWalletChecking}
      checkWalletError={checkWalletError}
    />
  </View>
);

const styles = StyleSheet.create({
  walletsList: {
    flex: 1,
  },
});

const mapStateToProps = (state) => ({
  displayMode: getDisplayMode(state),
  currency: getCurrencySelector(state),
  groupedWallets: getGroupedWallets(state),
  roundWallets: getRoundWalletsSelector(state),
  checkWalletError: getCheckWalletError(state),
  isWalletChecking: getIsWalletChecking(state),
  walletForEditing: getWalletForEditing(state),
  isBalancesLoading: getIsBalancesLoading(state),
  isAddWalletModalOpened: getIsAddWalletModalOpened(state),
});

const mapDispatchToProps = {
  addWallet: addWalletAction,
  deleteWallet: deleteWalletAction,
  updateWallet: updateWalletAction,
  openAddWalletModal: openAddWalletModalAction,
  closeAddWalletModal: closeAddWalletModalAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(WalletsList);
