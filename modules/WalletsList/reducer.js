import {
  OPEN_ADD_WALLET_MODAL,
  CLOSE_ADD_WALLET_MODAL,
  ADD_WALLET_START,
  ADD_WALLET_FAIL,
  CHANGE_WALLETS,
  LOAD_BALANCES_START,
  LOAD_BALANCES_SUCCESS,
  LOAD_BALANCES_FAIL,
  SAVE_RATES,
} from './constants';

const initialState = {
  wallets: [],
  rates: null,
  ratesValidityDate: null,
  isAddWalletModalOpened: false,
  checkWalletError: null,
  isWalletChecking: false,
  isBalancesLoading: false,
  walletForEditing: null,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case OPEN_ADD_WALLET_MODAL:
      return {
        ...state,
        isAddWalletModalOpened: true,
        walletForEditing: payload || null,
      };

    case CLOSE_ADD_WALLET_MODAL:
      return {
        ...state,
        checkWalletError: null,
        isWalletChecking: false,
        walletForEditing: null,
        isAddWalletModalOpened: false,
      };

    case ADD_WALLET_START:
      return {
        ...state,
        isWalletChecking: true,
      };

    case ADD_WALLET_FAIL:
      return {
        ...state,
        isWalletChecking: false,
        checkWalletError: payload,
      };

    case CHANGE_WALLETS:
      return {
        ...state,
        wallets: payload.wallets,
        checkWalletError: null,
        isWalletChecking: false,
        walletForEditing: null,
        isAddWalletModalOpened: false,
      };

    case LOAD_BALANCES_START:
      return {
        ...state,
        isBalancesLoading: true,
      };

    case LOAD_BALANCES_SUCCESS:
      return {
        ...state,
        isBalancesLoading: false,
        wallets: payload.wallets,
      };

    case LOAD_BALANCES_FAIL:
      return {
        ...state,
        isBalancesLoading: false,
      };

    case SAVE_RATES:
      return {
        ...state,
        rates: payload.rates,
        ratesValidityDate: payload.ratesValidityDate,
      };

    default:
      return state;
  }
};
