import {log, olderThanOneDay, calcBalancesForHistory} from '../../helpers';

import {
  ADD_WALLET_START,
  ADD_WALLET_FAIL,
  CHANGE_WALLETS,
  LOAD_BALANCES_START,
  LOAD_BALANCES_SUCCESS,
  LOAD_BALANCES_FAIL,
  OPEN_ADD_WALLET_MODAL,
  CLOSE_ADD_WALLET_MODAL,
  SAVE_RATES,
} from './constants';

import {getWallets, getRates, getRatesValidityDate} from './selectors';
import {getHistoryBalances} from '../Charts/selectors';
import {getCurrencySelector} from '../Account/selectors';
import {CURRENCIES} from '../Account/constants';

export const openAddWalletModalAction = (payload) => ({
  type: OPEN_ADD_WALLET_MODAL,
  payload,
});
export const closeAddWalletModalAction = () => ({type: CLOSE_ADD_WALLET_MODAL});

const updatePriceHelper = (wallet, balance, exchangePrices, fiatRate = 1) => {
  try {
    const exchangePrice =
      wallet.symbol === 'USDT' || wallet.symbol === 'USDC'
        ? 1
        : parseFloat(
            exchangePrices.find(({symbol}) => symbol === `${wallet.symbol}USDT`)
              ?.price || 1,
          );
    const usdtBalance = balance * exchangePrice;
    const fiat = usdtBalance * fiatRate;
    const balanceFiat = Math.floor(fiat * 100) / 100;
    return {...wallet, balance, usdtBalance, balanceFiat};
  } catch (error) {
    log(`ERROR in updatePriceHelper: ${error}`);
    return wallet;
  }
};

const loadBinancePrices = async () => {
  const res = await fetch('https://api.binance.com/api/v3/ticker/price');
  return res.json();
};

export const getBTCWalletBalance = async (address) => {
  try {
    // examle: xpub6CUGRUonZSQ4TWtTMmzXdrXDtypWKiKrhko4egpiMZbpiaQL2jkwSB1icqYh2cfDfVxdx4df189oLKnC5fSwqPfgyP3hooxujYzAu3fDVmz
    const url = `https://blockchain.info/multiaddr?active=${address}`;
    const res = await fetch(url);

    if (res.status === 200) {
      const btcInfo = await res.json();
      const balance = btcInfo?.wallet?.final_balance || 0;
      return `${balance / 100000000}`;
    } else {
      log(`ERROR in getBTCWalletBalance status: ${JSON.stringify(res || {})}`);
      return null;
    }
  } catch (error) {
    log(`ERROR in getBTCWalletBalance: ${error}`);
    return null;
  }
};

export const getXRPWalletBalance = async (address) => {
  try {
    const url = `https://data.ripple.com/v2/accounts/${address}/balances`;
    const res = await fetch(url);
    if (res.status === 200) {
      const info = await res.json();
      const balances = info?.balances || [];
      const balance = balances.reduce(
        (acc, b) => (acc += parseFloat(b.value)),
        0,
      );
      return balance;
    } else {
      log(`ERROR in getXRPWalletBalance status: ${JSON.stringify(res || {})}`);
      return null;
    }
  } catch (error) {
    log(`ERROR in getXRPWalletBalance: ${error}`);
    return null;
  }
};

export const getETHWalletBalance = async (address) => {
  try {
    const url = `https://api.blockcypher.com/v1/eth/main/addrs/${address}`;
    const res = await fetch(url);
    if (res.status === 200) {
      const info = await res.json();
      const balance = (info?.final_balance || 0) / 1000000000000000000;
      return balance;
    } else {
      log(`ERROR in getETHWalletBalance status: ${JSON.stringify(res || {})}`);
      return null;
    }
  } catch (error) {
    log(`ERROR in getETHWalletBalance: ${error}`);
    return null;
  }
};

const getFetchAction = (symbol) => {
  switch (symbol) {
    case 'BTC':
      return getBTCWalletBalance;
    case 'XRP':
      return getXRPWalletBalance;
    case 'ETH':
      return getETHWalletBalance;
    default:
      return null;
  }
};

export const addWalletAction = (wallet) => async (dispatch, getState) => {
  try {
    const state = getState();
    const savedWallets = getWallets(state);
    const name = wallet.name || `#${savedWallets?.length + 1}`;

    // For addresses no need to check for type, address is enough
    const isAmount = wallet.type === 'Amount';
    const currentWallet = isAmount
      ? `${wallet.symbol}${wallet.type}${wallet.balance}${wallet.name}`
      : `${wallet.symbol}${wallet.address}`;

    const isSaved = savedWallets.find((w) => {
      const isAmount = w.type === 'Amount';
      const checkWallet = isAmount
        ? `${w.symbol}${w.type}${w.balance}${w.name}`
        : `${w.symbol}${w.address}`;

      return checkWallet === currentWallet;
    });

    if (isSaved) {
      dispatch({type: ADD_WALLET_FAIL, payload: 'Wallet already exist'});
      return;
    }

    const exchangePrices = await loadBinancePrices();

    const rates = getRates(state);
    const savedCurrency = getCurrencySelector(state);
    const savedBalances = getHistoryBalances(state);
    const fiatRate = rates[savedCurrency];

    if (wallet.type === 'Amount') {
      // It's an number, no need to check it more, just save
      const newWallet = updatePriceHelper(
        {...wallet, name},
        parseFloat(wallet.balance),
        exchangePrices,
        fiatRate,
      );
      const newWallets = [...(savedWallets || []), newWallet];

      dispatch({
        type: CHANGE_WALLETS,
        payload: {
          wallets: newWallets,
          historyBalances: calcBalancesForHistory(savedBalances, newWallets),
        },
      });
    } else {
      dispatch({type: ADD_WALLET_START});

      // It's a public key, we should check if it's correct before saving
      const action = getFetchAction(wallet.symbol);

      if (action) {
        const balance = await action(wallet.address);

        if (balance !== null) {
          const newWallet = updatePriceHelper(
            {...wallet, balance, name},
            balance,
            exchangePrices,
            fiatRate,
          );
          const newWallets = [...(savedWallets || []), newWallet];

          dispatch({
            type: CHANGE_WALLETS,
            payload: {
              wallets: newWallets,
              historyBalances: calcBalancesForHistory(
                savedBalances,
                newWallets,
              ),
            },
          });
        } else {
          dispatch({
            type: ADD_WALLET_FAIL,
            payload: 'Invalid address provided',
          });
        }
      }
    }
  } catch (error) {
    dispatch({type: ADD_WALLET_FAIL, payload: 'Internal error'});
    log(`ERROR in addWalletAction: ${error}`);
  }
};

export const deleteWalletAction = (id) => (dispatch, getState) => {
  try {
    const state = getState();
    const savedWallets = getWallets(state);
    const savedBalances = getHistoryBalances(state);
    const newWallets = savedWallets.filter((w) => w.id !== id);
    const historyBalances = calcBalancesForHistory(savedBalances, newWallets);

    dispatch({
      type: CHANGE_WALLETS,
      payload: {wallets: newWallets, historyBalances},
    });
  } catch (error) {
    log(`ERROR in deleteWalletAction: ${error}`);
  }
};

export const getFiatRates = async (dispatch) => {
  try {
    const url =
      'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
    const res = await fetch(url);

    if (res.status === 200) {
      const crudeRates = await res.json();

      // UAH/USD info
      let {rate, exchangedate} = crudeRates.find(({cc}) => cc === 'USD') || {};

      const rates = crudeRates.reduce((acc, item) => {
        if (!CURRENCIES[item.cc]) return acc;

        if (item.cc === 'USD') {
          acc.UAH = item.rate;
        } else {
          acc[item.cc] = rate / item.rate;
        }

        return acc;
      }, {});

      dispatch({
        type: SAVE_RATES,
        payload: {
          rates,
          ratesValidityDate: exchangedate,
        },
      });

      return rates;
    } else {
      log(`ERROR in getFiatRates status: ${JSON.stringify(res || {})}`);
      return null;
    }
  } catch (error) {
    log(`ERROR in getFiatRates: ${error}`);
    return null;
  }
};

export const loadBalancesAction = () => async (dispatch, getState) => {
  try {
    const state = getState();
    const savedWallets = getWallets(state);
    const savedCurrency = getCurrencySelector(state);
    const ratesValidityDate = getRatesValidityDate(state);
    const savedBalances = getHistoryBalances(state);
    const needToGetNewRates =
      !ratesValidityDate || olderThanOneDay(ratesValidityDate);
    let rates = getRates(state);

    if (needToGetNewRates) {
      rates = await getFiatRates(dispatch);
    }

    if (!savedWallets || !savedWallets.length) return;

    dispatch({type: LOAD_BALANCES_START});

    const exchangePrices = await loadBinancePrices();

    const fetchBlock = await Promise.all(
      savedWallets
        .filter((w) => w.type !== 'Amount')
        .map((w) => {
          return new Promise(async (resolve) => {
            const action = getFetchAction(w.symbol);

            if (action) {
              const fetchedBalance = await action(w.address);
              const balance = fetchedBalance || w.balance;
              resolve({id: w.id, balance});
            }
          });
        }),
    );

    const fiatRate = rates[savedCurrency];

    const newWallets = savedWallets.map((w) => {
      const fromBlock = fetchBlock.find((f) => f.id === w.id);
      const balance = fromBlock?.balance || w.balance;

      return updatePriceHelper(w, balance, exchangePrices, fiatRate);
    });

    const historyBalances = calcBalancesForHistory(savedBalances, newWallets);

    dispatch({
      type: LOAD_BALANCES_SUCCESS,
      payload: {wallets: newWallets, historyBalances},
    });
  } catch (error) {
    dispatch({type: LOAD_BALANCES_FAIL});
    log(`ERROR in loadBalancesAction: ${error}`);
  }
};

export const recalculateFiatPricesAction = (savedCurrency) => async (
  dispatch,
  getState,
) => {
  try {
    const state = getState();
    const rates = getRates(state);
    const savedWallets = getWallets(state);
    const savedBalances = getHistoryBalances(state);

    if (!savedWallets || !savedWallets.length) return;

    const fiatRate = rates[savedCurrency] || 1;

    const newWallets = savedWallets.map((w) => {
      const fiat = w.usdtBalance * fiatRate;
      const balanceFiat = Math.floor(fiat * 100) / 100;
      return {...w, balanceFiat};
    });

    const historyBalances = calcBalancesForHistory(savedBalances, newWallets);

    dispatch({
      type: LOAD_BALANCES_SUCCESS,
      payload: {wallets: newWallets, historyBalances},
    });
  } catch (error) {
    dispatch({type: LOAD_BALANCES_FAIL});
    log(`ERROR in recalculateFiatPricesAction: ${error}`);
  }
};

export const updateWalletAction = (wallet, name, value, isColdWallet) => (
  dispatch,
  getState,
) => {
  try {
    const isAmount = wallet.type === 'Amount';

    // Check first if something was changed or not
    const sameName = name === wallet.name;
    const sameBalance = value === wallet.balance;
    const sameIsColdWallet = isColdWallet === wallet.isColdWallet;
    const notUpdated = isAmount
      ? sameName && sameIsColdWallet && sameBalance
      : sameName && sameIsColdWallet;

    if (notUpdated) {
      dispatch(closeAddWalletModalAction());
      return;
    }

    const state = getState();
    const savedWallets = getWallets(state);
    const savedBalances = getHistoryBalances(state);

    const walletIndex = savedWallets.findIndex(({id}) => id === wallet.id);
    const updatedWallet = {
      ...wallet,
      ...(isAmount
        ? {
            name: name || wallet.name,
            balance: value || wallet.balance,
            isColdWallet: isColdWallet,
          }
        : {name: name || wallet.name, isColdWallet: isColdWallet}),
    };
    const newWallets = [
      ...savedWallets.slice(0, walletIndex),
      updatedWallet,
      ...savedWallets.slice(walletIndex + 1),
    ];
    const historyBalances = calcBalancesForHistory(savedBalances, newWallets);

    dispatch(closeAddWalletModalAction());
    dispatch({
      type: CHANGE_WALLETS,
      payload: {wallets: newWallets, historyBalances},
    });

    // Balance was changed!!! Wait for updated wallets to be saved, then re-update prices
    if (isAmount && !sameBalance) {
      setTimeout(() => loadBalancesAction()(dispatch, getState), 500);
    }
  } catch (error) {
    log(`ERROR in updateWalletAction: ${error}`);
  }
};
