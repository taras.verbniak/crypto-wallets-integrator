import React, {useState, useEffect} from 'react';
import {View, Image, Text, StyleSheet, TextInput} from 'react-native';
import {Overlay, Button, CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import IconAwesome from 'react-native-vector-icons/dist/FontAwesome5';
import ModalDropdown from 'react-native-modal-dropdown';
import {withTranslation} from 'react-i18next';

import {isNumber} from '../../../helpers';
import {COINS_LIST, TYPE_LIST} from '../constants';

const AddWalletModal = ({
  t,
  show,
  close,
  addWallet,
  updateWallet,
  deleteWallet,
  walletForEditing,
  isWalletChecking,
  checkWalletError,
}) => {
  const isEditing = !!walletForEditing;

  const [coin, setCoin] = useState(null);
  const [type, setType] = useState(null);
  const [value, setValue] = useState('');
  const [name, setName] = useState('');
  const [defaultValue, setDefaultValue] = useState('');
  const [defaultName, setDefaultName] = useState('');
  const [isColdWallet, setIsColdWallet] = useState(false);
  const [showCoinError, setShowCoinError] = useState(null);
  const [showTypeError, setShowTypeError] = useState(null);
  const [showAddressError, setShowAddressError] = useState(null);
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const [stableCoinTypeError, setStableCoinTypeError] = useState(null);

  const isAmount = type === 'Amount';

  useEffect(() => {
    if (show) {
      setValue(isEditing ? walletForEditing?.balance : '');
      setName(isEditing ? walletForEditing?.name : '');
      setCoin(null);
      setShowCoinError(null);
      setShowTypeError(null);
      setShowAddressError(null);
      setShowConfirmDelete(false);
      setStableCoinTypeError(null);
      setType(isEditing ? walletForEditing?.type : null);
      setDefaultName(isEditing ? walletForEditing?.name : '');
      setDefaultValue(isEditing ? walletForEditing?.balance : '');
      setIsColdWallet(isEditing ? walletForEditing?.isColdWallet : false);
    }
  }, [show]);

  const submitData = () => {
    if (isEditing) {
      if (type === 'Amount') {
        const parsedValue = parseFloat(value);

        if (isNaN(parsedValue) || parsedValue === 0) {
          setShowAddressError(true);
        } else {
          updateWallet(walletForEditing, name, parsedValue, isColdWallet);
        }
      } else {
        updateWallet(walletForEditing, name, null, isColdWallet);
      }
    } else {
      if (!coin) setShowCoinError(true);
      if (!type) setShowTypeError(true);
      if (!value) setShowAddressError(true);

      if (coin && type && value) {
        const id = Math.random().toString(16).slice(2);
        const isStableCoin = coin.symbol === 'USDT' || coin.symbol === 'USDC';

        if (isAmount) {
          const validNumber = isNumber(value);
          if (validNumber && value > 0) {
            addWallet({
              id,
              type,
              name,
              isColdWallet,
              balance: value,
              ...coin,
            });
          } else {
            setShowAddressError(true);
          }
        } else if (isStableCoin) {
          setShowTypeError(true);
          setStableCoinTypeError(
            'USDT/USDC supports only Amount type of wallet',
          );
        } else {
          addWallet({
            id,
            type,
            name,
            isColdWallet,
            address: value,
            ...coin,
          });
        }
      }
    }
  };

  const selectCoin = (_coin) => {
    setCoin(_coin);
    setShowCoinError(null);
  };

  const selectType = (_type) => {
    setType(_type);
    setShowTypeError(null);
    setStableCoinTypeError(null);
  };

  const onInputChange = (value) => {
    setValue(value);
    setShowAddressError(null);
  };

  const renderTypesRow = (title, iconName, isList) => (
    <View style={{...styles.dropdownItem, ...(isList ? {padding: 15} : {})}}>
      <IconAwesome name={iconName} size={25} color="#fff" />
      <Text style={styles.dropdownItemText}>{t(title)}</Text>
    </View>
  );

  const renderCoinsRow = (title, iconSource, isList) => (
    <View style={{...styles.dropdownItem, ...(isList ? {padding: 15} : {})}}>
      <Image source={iconSource} style={styles.dropdownItemIcon} />
      <Text style={styles.dropdownItemText}>{title}</Text>
    </View>
  );

  const localErrorMsg = isAmount
    ? 'Please enter correct number (with . separator)'
    : stableCoinTypeError || 'Please enter valid address';

  let addressErrorText = showAddressError ? localErrorMsg : null;
  addressErrorText = type ? addressErrorText : null;

  const submitDisabled = showCoinError || showTypeError || !!addressErrorText;

  addressErrorText = checkWalletError || addressErrorText;

  return (
    <Overlay
      isVisible={show}
      onBackdropPress={() => close(false)}
      overlayStyle={styles.overlay}
      backdropStyle={styles.overlayBackdrop}>
      <>
        {showConfirmDelete ? (
          <View style={{marginHorizontal: 10, marginTop: 0}}>
            <Text style={{color: '#fff', fontSize: 17, textAlign: 'center'}}>
              {t('Are you sure you want to delete wallet?')}
            </Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <Button
                onPress={() => deleteWallet(walletForEditing.id)}
                title={t('Yes')}
                containerStyle={{marginTop: 20}}
                buttonStyle={{
                  width: 70,
                  paddingVertical: 10,
                  backgroundColor: 'rgba(255,0,0,0.7)',
                }}
              />
              <Button
                onPress={() => setShowConfirmDelete(false)}
                title={t('No')}
                containerStyle={{marginTop: 20}}
                buttonStyle={{
                  width: 70,
                  paddingVertical: 10,
                }}
              />
            </View>
          </View>
        ) : (
          <>
            {!isEditing && (
              <>
                <View style={styles.dropdownWrapper}>
                  <ModalDropdown
                    options={TYPE_LIST}
                    style={{
                      ...styles.dropdown,
                      borderBottomColor: showTypeError ? 'red' : '#fff',
                    }}
                    textStyle={{fontSize: 10, backgroundColor: 'red'}}
                    defaultValue={t('Select wallet type...')}
                    dropdownStyle={{...styles.dropdownList, height: 113}}
                    onSelect={(_, {title}) => selectType(title)}
                    textStyle={{
                      ...styles.dropdownBtnText,
                      color: coin ? '#fff' : '#7d7d7d',
                    }}
                    renderButtonText={({title, iconName}) =>
                      renderTypesRow(title, iconName)
                    }
                    renderRow={({title, iconName}) =>
                      renderTypesRow(title, iconName, true)
                    }
                  />
                  <Icon
                    style={styles.dropdownArrow}
                    name="chevron-down"
                    size={40}
                    color="#fff"
                  />
                  {showTypeError && (
                    <Text style={styles.dropdownError}>
                      {t(stableCoinTypeError || 'No type selected')}
                    </Text>
                  )}
                  {isAmount && (
                    <Text style={styles.dropdownWarning}>
                      {t(
                        'should be updated manually after each balance change',
                      )}
                    </Text>
                  )}
                </View>
                <View style={styles.dropdownWrapper}>
                  <ModalDropdown
                    options={COINS_LIST}
                    style={{
                      ...styles.dropdown,
                      borderBottomColor: showCoinError ? 'red' : '#fff',
                    }}
                    defaultValue={t('Select coin...')}
                    dropdownStyle={{...styles.dropdownList, minHeight: 303}}
                    onSelect={(_, {title, symbol}) =>
                      selectCoin({title, symbol})
                    }
                    textStyle={{
                      ...styles.dropdownBtnText,
                      color: coin ? '#fff' : '#7d7d7d',
                    }}
                    renderButtonText={({title, iconSource}) =>
                      renderCoinsRow(title, iconSource)
                    }
                    renderRow={({title, iconSource}) =>
                      renderCoinsRow(title, iconSource, true)
                    }
                  />
                  <Icon
                    style={styles.dropdownArrow}
                    name="chevron-down"
                    size={40}
                    color="#fff"
                  />
                  {showCoinError && (
                    <Text style={styles.dropdownError}>
                      {t('No coin selected')}
                    </Text>
                  )}
                </View>
              </>
            )}
            <View style={{marginHorizontal: 10, marginTop: 20}}>
              <Text style={styles.inputLabel}>{`${t('Wallet name')} ${
                isEditing ? '' : t('(optional)')
              }`}</Text>
              <TextInput
                autoCorrect={false}
                style={styles.input}
                onChangeText={(n) => setName(n)}
                defaultValue={`${defaultName}`}
              />
            </View>
            {(!isEditing || (isEditing && isAmount)) && (
              <View style={{marginHorizontal: 10, marginTop: 40}}>
                <Text style={styles.inputLabel}>
                  {t(type ? (!isAmount ? 'Address' : 'Amount') : null)}
                </Text>
                <TextInput
                  autoCorrect={false}
                  autoCompleteType="off"
                  keyboardType="number-pad"
                  placeholderTextColor="#c5c5c5"
                  defaultValue={`${defaultValue}`}
                  onChangeText={(value) => onInputChange(value)}
                  style={{
                    ...styles.input,
                    borderBottomColor:
                      showAddressError || addressErrorText ? 'red' : '#fff',
                  }}
                />
                {addressErrorText && (
                  <Text style={styles.inputError}>{t(addressErrorText)}</Text>
                )}
              </View>
            )}
            <View style={styles.checkboxWrapper}>
              <CheckBox
                title={t('Cold wallet')}
                checked={isColdWallet}
                textStyle={styles.checkboxText}
                containerStyle={styles.checkbox}
                onPress={() => setIsColdWallet(!isColdWallet)}
                iconType="material"
                checkedIcon="check-box"
                uncheckedColor="#9e9e9e"
                uncheckedIcon="check-box-outline-blank"
              />
            </View>
            <Button
              onPress={submitData}
              title={t(isEditing ? 'Save changes' : 'Save')}
              disabled={submitDisabled}
              disabledStyle={{backgroundColor: '#c5c5c5', opacity: 0.3}}
              loading={isWalletChecking}
              containerStyle={{marginTop: 50}}
              buttonStyle={styles.buttonStyle}
            />
            {isEditing && (
              <Button
                onPress={() => setShowConfirmDelete(true)}
                title={t('Delete wallet')}
                containerStyle={{marginTop: 20}}
                buttonStyle={{
                  ...styles.buttonStyle,
                  backgroundColor: 'rgba(255,0,0,0.7)',
                }}
              />
            )}
          </>
        )}
      </>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  checkboxWrapper: {
    marginTop: 20,
    marginHorizontal: 10,
  },
  checkbox: {
    width: '100%',
    marginLeft: 0,
    paddingLeft: 0,
    borderWidth: 0,
    backgroundColor: 'transparent',
  },
  checkboxText: {
    fontSize: 16,
    fontWeight: '600',
    color: '#9e9e9e',
  },
  overlay: {
    width: '90%',
    backgroundColor: '#141f2e',
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderRadius: 9,
  },
  overlayBackdrop: {
    backgroundColor: '#7e7e7e',
    opacity: 0.7,
  },
  dropdownWrapper: {
    marginBottom: 30,
  },
  dropdownBtnText: {fontSize: 18},
  dropdownArrow: {
    position: 'absolute',
    right: 0,
    top: 4,
    zIndex: -1,
  },
  dropdown: {
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
  },
  dropdownError: {
    position: 'absolute',
    left: 10,
    bottom: -20,
    color: 'red',
    fontSize: 12,
  },
  dropdownWarning: {
    position: 'absolute',
    left: 10,
    top: 50,
    paddingRight: 10,
    color: 'yellow',
    fontSize: 12,
  },
  dropdownItemIcon: {height: 30, width: 30},
  dropdownList: {
    backgroundColor: '#5a616d',
    borderWidth: 0,
    borderRadius: 5,
    width: '80%',
  },
  dropdownItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: 18,
    marginLeft: 10,
    color: '#fff',
  },
  inputLabel: {
    color: '#9e9e9e',
    fontSize: 16,
    position: 'absolute',
    bottom: 35,
  },
  inputError: {
    position: 'absolute',
    fontSize: 12,
    color: 'red',
    top: 45,
  },
  input: {
    height: 40,
    color: '#fff',
    fontSize: 18,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
  },
  buttonStyle: {padding: 15},
});

export default withTranslation()(AddWalletModal);
