import React from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {withTranslation} from 'react-i18next';
import Icon from 'react-native-vector-icons/dist/EvilIcons';

const AddWalletButton = ({t, openAddWalletModal}) => (
  <View style={styles.btnWrapper}>
    <TouchableWithoutFeedback onPress={() => openAddWalletModal()}>
      <View style={styles.btn}>
        <Icon name="plus" size={30} color="#000" style={styles.icon} />
        <Text style={styles.btnText}>{t('Add new wallet')}</Text>
      </View>
    </TouchableWithoutFeedback>
  </View>
);

const styles = StyleSheet.create({
  btnWrapper: {
    paddingTop: 25,
    marginBottom: 50,
    alignItems: 'center',
  },
  btn: {
    borderColor: 'rgba(197, 197, 197, 0.5)',
    borderWidth: 1,
    width: '80%',
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: '#c5c5c5',
    fontSize: 16,
    marginLeft: 3,
  },
  icon: {
    color: '#c5c5c5',
  },
});

export default withTranslation()(AddWalletButton);
