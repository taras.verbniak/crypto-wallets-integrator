import React from 'react';
import {View, Text} from 'react-native';

import {getNumberChunks} from '../../../helpers';

const NumberFormatted = ({
  round,
  color,
  number,
  currency,
  fontSize,
  fontWeight,
  textStyles,
  allDecimals,
  chunkMargin,
  hiddenNumbers,
  containerStyles,
}) => {
  const chunks = !hiddenNumbers && getNumberChunks(number, round, allDecimals);
  const totalText = (t, marginLeft = chunkMargin, marginRight = 0) => (
    <Text
      key={t}
      style={{
        color,
        fontSize,
        fontWeight,
        marginLeft,
        marginRight,
        ...textStyles,
      }}>
      {t}
    </Text>
  );

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        ...containerStyles,
      }}>
      {hiddenNumbers ? totalText('*****') : chunks.map((t) => totalText(t))}
      {totalText(currency, chunkMargin * 1.5, chunkMargin)}
    </View>
  );
};

NumberFormatted.defaultProps = {
  color: '#fff',
  currency: 'USD',
  fontSize: 32,
  chunkMargin: 10,
  fontWeight: 'normal',
  textStyles: {},
  containerStyles: {},
};

export default NumberFormatted;
