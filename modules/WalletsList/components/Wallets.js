import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {Image} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {withTranslation} from 'react-i18next';

import AddWalletButton from './AddWalletButton';
import NumberFormatted from './NumberFormatted';
import IconAwesome from 'react-native-vector-icons/dist/FontAwesome5';

import {COINS} from '../constants';

const Wallets = ({
  t,
  wallets,
  currency,
  displayMode,
  roundWallets,
  isBalancesLoading,
  openAddWalletModal,
}) => {
  if (!Array.isArray(wallets)) return null;

  return (
    <FlatList
      style={styles.container}
      data={[...(wallets || []), {type: 'add_btn'}]}
      renderItem={({item}) => {
        if (item.type === 'add_btn') {
          return <AddWalletButton openAddWalletModal={openAddWalletModal} />;
        }

        const blockTotal = item?.reduce((acc, w) => (acc += w.balance), 0);
        let blockTotalFiat = item?.reduce(
          (acc, w) => (acc += w.balanceFiat),
          0,
        );
        blockTotalFiat = Math.floor(blockTotalFiat * 100) / 100;

        return (
          <View style={styles.block}>
            <LinearGradient
              colors={['#0e0e0e', '#373b4f']}
              end={{x: 0, y: 0}}
              start={{x: 0, y: 1}}
              style={styles.blockTitle}>
              <View style={styles.blockTitleTextWrapper}>
                <Image
                  source={COINS[item[0].symbol]?.iconSource}
                  style={{height: 30, width: 30}}
                />
                <Text style={styles.blockTitleText}>{item[0]?.title}</Text>
              </View>
              <View style={{alignItems: 'flex-end'}}>
                {isBalancesLoading ? (
                  <Text style={{...styles.blockTotal, marginBottom: 3}}>
                    {t('Loading...')}
                  </Text>
                ) : (
                  <NumberFormatted
                    allDecimals={
                      item[0]?.symbol === 'BTC' || item[0]?.symbol === 'ETH'
                    }
                    fontSize={16}
                    chunkMargin={7}
                    number={
                      item[0]?.symbol === 'ETH'
                        ? blockTotal?.toFixed(3)
                        : blockTotal
                    }
                    currency={item[0]?.symbol}
                    hiddenNumbers={!!displayMode}
                    textStyles={{color: '#a4baef'}}
                    containerStyles={{marginBottom: 3}}
                  />
                )}
                {isBalancesLoading ? (
                  <Text style={styles.blockTotal}>{t('Loading...')}</Text>
                ) : (
                  <NumberFormatted
                    fontSize={16}
                    chunkMargin={7}
                    currency={currency}
                    round={roundWallets}
                    number={blockTotalFiat}
                    hiddenNumbers={!!displayMode}
                  />
                )}
              </View>
            </LinearGradient>
            {item.map((wallet) => {
              const isAmount = wallet.type === 'Amount';
              return (
                <LinearGradient
                  key={wallet.id}
                  colors={['#608a88', '#1360a8']}
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 1}}
                  style={styles.wallet}>
                  <Text style={styles.walletName}>
                    {displayMode === 2 ? '*******' : wallet.name}
                  </Text>
                  <IconAwesome
                    name={isAmount ? 'database' : 'wallet'}
                    size={20}
                    style={styles.walletType}
                    color="#c5c5c5"
                  />
                  <View style={styles.walletRightBlock}>
                    <View style={styles.walletPrice}>
                      {isBalancesLoading ? (
                        <Text style={styles.walletPriceBalance}>
                          {t('Loading...')}
                        </Text>
                      ) : (
                        <NumberFormatted
                          allDecimals={
                            item[0]?.symbol === 'BTC' ||
                            item[0]?.symbol === 'ETH'
                          }
                          fontSize={14}
                          chunkMargin={5}
                          textStyles={{color: '#a4baef'}}
                          number={
                            item[0]?.symbol === 'ETH'
                              ? wallet?.balance?.toFixed(3)
                              : wallet?.balance
                          }
                          currency={wallet.symbol}
                          hiddenNumbers={!!displayMode}
                        />
                      )}
                      {isBalancesLoading ? (
                        <Text style={styles.walletPriceBalanceFiat}>
                          {t('Loading...')}
                        </Text>
                      ) : (
                        <NumberFormatted
                          fontSize={14}
                          chunkMargin={5}
                          currency={currency}
                          round={roundWallets}
                          number={wallet.balanceFiat}
                          hiddenNumbers={!!displayMode}
                        />
                      )}
                    </View>
                    <TouchableOpacity
                      onPress={() => openAddWalletModal(wallet)}>
                      <View
                        style={{
                          paddingVertical: 10,
                          paddingRight: 10,
                          paddingLeft: 3,
                        }}>
                        <IconAwesome
                          name="pencil-alt"
                          size={14}
                          style={{opacity: 0.6}}
                          color="#c5c5c5"
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </LinearGradient>
              );
            })}
          </View>
        );
      }}
      keyExtractor={(item) => item[0]?.id || item.type}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  block: {
    paddingVertical: 0,
    marginVertical: 20,
  },
  blockTitle: {
    borderRadius: 7,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#5d5d5d',
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginBottom: 5,
  },
  blockTitleTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  blockTitleText: {
    marginLeft: 10,
    color: '#fff',
    fontSize: 18,
  },
  blockTotal: {
    color: '#fff',
    fontSize: 16,
    alignItems: 'flex-end',
  },
  wallet: {
    borderRadius: 7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8,
    paddingLeft: 10,
    marginBottom: 5,
    backgroundColor: 'green',
  },
  walletName: {
    flex: 4,
    color: '#fff',
    fontSize: 16,
    paddingRight: 5,
  },
  walletType: {
    flex: 1,
  },
  walletRightBlock: {
    flex: 6,
    flexDirection: 'row',
  },
  walletPrice: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  walletPriceBalance: {
    color: '#fff',
  },
  walletPriceBalanceFiat: {
    color: '#fff',
  },
  walletButtons: {
    justifyContent: 'space-between',
  },
});

export default withTranslation()(Wallets);
