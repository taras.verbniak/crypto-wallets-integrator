import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import Home from '../Home/Home';
import Charts from '../Charts/Charts';
import Navbar from '../Navbar/Navbar';
import Account from '../Account/Account';

const Root = () => (
  <LinearGradient
    colors={['#03060f', '#1360a8']}
    end={{x: 0, y: 0}}
    start={{x: 0, y: 1}}
    style={styles.container}>
    <Home />
    <Charts />
    <Account />
    <Navbar />
  </LinearGradient>
);

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'space-between',
  },
});

export default Root;
