import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Overlay, Button, Tab} from 'react-native-elements';
import IconAwesome from 'react-native-vector-icons/dist/FontAwesome5';
import {withTranslation} from 'react-i18next';

import {pickFile} from '../../../helpers';
import {BACKUP_FILE_NAME, BACKUP_DIRECTORY} from '../constants';

const ImportExportModal = ({
  t,
  show,
  close,
  isWalletChecking,
  importExportError,
  startImportExport,
  importExportSuccess,
}) => {
  const [error, setError] = useState(null);
  const [isExport, setIsExport] = useState(true);
  const [importFileName, setImportFileName] = useState(BACKUP_FILE_NAME);

  const setErrorWithTimeout = (err) => {
    setError(err);
    setTimeout(() => setError(null), 3000);
  };

  useEffect(() => {
    if (show) {
      setError(null);
      setIsExport(true);
      setImportFileName(null);
    }
  }, [show]);

  const pickFileHandler = () => {
    pickFile()
      .then((fileName, err) => {
        if (fileName) {
          setImportFileName(fileName);
        } else {
          setErrorWithTimeout(err);
        }
      })
      .catch((err) => setErrorWithTimeout(err));
  };

  return (
    <Overlay
      isVisible={show}
      onBackdropPress={() => close(false)}
      overlayStyle={styles.overlay}
      backdropStyle={styles.overlayBackdrop}>
      <>
        <Tab
          value={isExport ? 0 : 1}
          indicatorStyle={{backgroundColor: '#c5c5c5'}}
          onChange={(v) => setIsExport(v === 0 ? true : false)}>
          <Tab.Item
            title={t('Export')}
            titleStyle={{color: '#c5c5c5', backgroundColor: 'transparent'}}
            containerStyle={{backgroundColor: 'transparent'}}
            containerStyle={
              isExport ? {backgroundColor: 'rgba(256,256,256,0.15)'} : {}
            }
          />
          <Tab.Item
            title={t('Import')}
            titleStyle={{color: '#c5c5c5', backgroundColor: 'transparent'}}
            containerStyle={{backgroundColor: 'transparent'}}
            containerStyle={
              !isExport ? {backgroundColor: 'rgba(256,256,256,0.15)'} : {}
            }
          />
        </Tab>
        <View
          style={{
            marginHorizontal: 10,
            marginVertical: 20,
          }}>
          {isExport ? (
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                margin: 10,
                lineHeight: 25,
              }}>
              {`${t(
                'Click "Start" to export data into',
              )} "${BACKUP_DIRECTORY}/${BACKUP_FILE_NAME}"`}
            </Text>
          ) : (
            <View>
              <Text
                style={{
                  color: 'white',
                  marginVertical: 5,
                }}>
                {`${t(
                  'Important!! Your file should be inside',
                )} "${BACKUP_DIRECTORY}/"`}
              </Text>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                }}>
                <Button
                  type="outline"
                  onPress={pickFileHandler}
                  title={t('Choose file')}
                  titleStyle={{color: '#c5c5c5'}}
                  containerStyle={{marginVertical: 10}}
                  buttonStyle={{borderColor: '#c5c5c5', width: 150}}
                  icon={
                    <IconAwesome
                      size={20}
                      color="#c5c5c5"
                      name="folder-open"
                      style={{marginRight: 5}}
                    />
                  }
                />
                {importFileName && (
                  <Text
                    style={{
                      color: '#c5c5c5',
                      marginLeft: 10,
                      marginBottom: 10,
                    }}>{`.../${importFileName}`}</Text>
                )}
              </View>
            </View>
          )}
          {(error || importExportError) && (
            <Text style={{color: 'red', fontSize: 16, marginTop: 10}}>
              {t(error || importExportError)}
            </Text>
          )}
        </View>

        <Button
          onPress={() => startImportExport(!isExport, importFileName)}
          title={t(
            importExportSuccess
              ? `${isExport ? 'Export' : 'Import'} successful!`
              : 'Start',
          )}
          disabled={importExportSuccess}
          disabledTitleStyle={{color: 'green'}}
          disabledStyle={{backgroundColor: '#141f2e'}}
          loading={isWalletChecking}
          containerStyle={{marginTop: 10}}
          buttonStyle={styles.buttonStyle}
        />
      </>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  overlay: {
    width: '90%',
    backgroundColor: '#141f2e',
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderRadius: 9,
  },
  overlayBackdrop: {
    backgroundColor: '#7e7e7e',
    opacity: 0.7,
  },
  buttonStyle: {padding: 15},
});

export default withTranslation()(ImportExportModal);
