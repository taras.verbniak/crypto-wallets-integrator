import React from 'react';
import {View, Image, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import ModalDropdown from 'react-native-modal-dropdown';
import {CheckBox} from 'react-native-elements';
import {withTranslation} from 'react-i18next';

import {openLink} from '../../../helpers';

import {
  PRIVACY_URL,
  COUNTRIES,
  CURRENCIES,
  COUNTRIES_LIST,
  CURRENCIES_LIST,
  DISPLAY_MODES_LIST,
} from '../constants';

const AppInfo = ({
  t,
  i18n,
  currency,
  roundTotal,
  setCurrency,
  displayMode,
  setLanguage,
  roundWallets,
  setDisplayMode,
  toggleRoundTotal,
  toggleRoundWallets,
  toggleImportExportModal,
}) => {
  const renderCoinsRow = (title, icon, isList) => (
    <View style={{...styles.dropdownItem, ...(isList ? {padding: 12} : {})}}>
      <Image source={icon} style={styles.dropdownItemIcon} />
      <Text style={styles.dropdownItemText}>{title}</Text>
    </View>
  );
  const currentDisplayMode =
    DISPLAY_MODES_LIST.find(({value}) => value === displayMode) ||
    DISPLAY_MODES_LIST[0];

  const currentLang = COUNTRIES[i18n?.language || 'en'];
  const langItem = renderCoinsRow(currentLang.title, currentLang.icon);

  const selectedCurrency = CURRENCIES[currency || 'USD'];

  const currencyItem = (
    <Text style={styles.dropdownItemText}>
      {`${selectedCurrency?.currencyId} - ${selectedCurrency?.currencyName}`}
    </Text>
  );

  return (
    <View style={styles.container}>
      <View style={styles.infoContainer}>
        <Image
          source={require('../../../icons/cwi-logo.png')}
          style={styles.icon}
        />
        <Text style={styles.text}>v. 2.9.0</Text>
      </View>
      <View style={{width: '100%', paddingBottom: 10}}>
        <CheckBox
          title={t('Round total fiat balance')}
          checked={roundTotal}
          textStyle={styles.checkboxText}
          containerStyle={styles.checkbox}
          onPress={toggleRoundTotal}
          iconType="material"
          checkedIcon="check-box"
          uncheckedIcon="check-box-outline-blank"
        />
        <CheckBox
          title={t('Round blocks/wallets fiat balances')}
          checked={roundWallets}
          textStyle={styles.checkboxText}
          containerStyle={styles.checkbox}
          onPress={toggleRoundWallets}
          iconType="material"
          checkedIcon="check-box"
          uncheckedIcon="check-box-outline-blank"
        />
      </View>
      <View style={styles.dropdownWrapper}>
        <ModalDropdown
          options={COUNTRIES_LIST}
          defaultValue=" "
          dropdownStyle={styles.dropdownList}
          textStyle={styles.dropdownTextStyle}
          renderButtonText={() => <Text> </Text>}
          renderRow={({title, icon}) => renderCoinsRow(title, icon, true)}
          onSelect={(_, {locale}) => {
            setLanguage(locale);
            i18n.changeLanguage(locale);
          }}
        />
        <Text style={styles.dropdownLabel}>{`${t('Language')}:`}</Text>
        <View style={styles.currentLanguage}>{langItem}</View>
        <Icon
          style={styles.dropdownArrow}
          name="chevron-down"
          size={40}
          color="#c5c5c5"
        />
      </View>
      <View style={styles.dropdownWrapper}>
        <ModalDropdown
          options={CURRENCIES_LIST}
          defaultValue=" "
          dropdownStyle={{...styles.dropdownList, minHeight: 300}}
          textStyle={styles.dropdownTextStyle}
          renderButtonText={() => <Text> </Text>}
          renderRow={({currencyId, currencyName}) => (
            <View
              style={{
                ...styles.dropdownItem,
                ...{
                  padding: 12,
                  borderBottomWidth: currencyId === 'AUD' ? 10 : 0,
                },
              }}>
              <Text style={styles.dropdownItemText}>
                {`${currencyId} - ${currencyName}`}
              </Text>
            </View>
          )}
          onSelect={(_, {currencyId}) => setCurrency(currencyId)}
        />
        <Text style={styles.dropdownLabel}>{`${t('Base currency')}:`}</Text>
        <View style={styles.selectedCurrency}>{currencyItem}</View>
        <Icon
          style={styles.dropdownArrow}
          name="chevron-down"
          size={40}
          color="#c5c5c5"
        />
      </View>
      <View style={styles.dropdownWrapper}>
        <ModalDropdown
          options={DISPLAY_MODES_LIST}
          defaultValue=" "
          dropdownStyle={{...styles.dropdownList, minHeight: 10, height: 127}}
          textStyle={styles.dropdownTextStyle}
          renderButtonText={() => <Text> </Text>}
          renderRow={({value, label}) => (
            <View
              style={{
                ...styles.dropdownItem,
                ...{padding: 12, borderBottomWidth: 0},
              }}>
              <Text style={styles.dropdownItemText}>{t(label)}</Text>
            </View>
          )}
          onSelect={(_, {value}) => setDisplayMode(value)}
        />
        <Text style={styles.dropdownLabel}>{`${t('Display mode')}:`}</Text>
        <View style={styles.selectedCurrency}>
          <Text style={styles.dropdownItemText}>
            {t(currentDisplayMode.label)}
          </Text>
        </View>
        <Icon
          style={styles.dropdownArrow}
          name="chevron-down"
          size={40}
          color="#c5c5c5"
        />
      </View>
      <TouchableHighlight onPress={() => toggleImportExportModal(true)}>
        <View style={{marginTop: 30}}>
          <Text style={styles.privacyBtnText}>{t('Import/Export data')}</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => openLink(PRIVACY_URL)}>
        <View style={styles.privacyBtn}>
          <Text style={styles.privacyBtnText}>{t('Privacy Policy')}</Text>
        </View>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  infoContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  icon: {
    height: 100,
    width: 100,
    borderRadius: 20,
  },
  text: {
    fontSize: 14,
    marginTop: 10,
    color: '#fff',
  },
  checkbox: {
    marginLeft: 0,
    width: '100%',
    borderWidth: 0,
    borderRadius: 7,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  checkboxText: {
    color: '#c5c5c5',
    paddingRight: 10,
  },
  privacyBtn: {
    marginTop: 10,
  },
  privacyBtnText: {
    color: '#8c8c8c',
    fontSize: 14,
  },
  dropdownWrapper: {
    width: '100%',
    marginTop: 30,
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: 45,
    borderRadius: 7,
    zIndex: -1,
  },
  dropdownItemIcon: {height: 30, width: 30},
  dropdownList: {
    backgroundColor: '#5a616d',
    borderWidth: 0,
    borderRadius: 5,
    width: '89%',
    minHeight: 167,
    zIndex: -1,
  },
  dropdownTextStyle: {
    position: 'relative',
    padding: 13,
  },
  dropdownItem: {
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: -1,
  },
  dropdownItemText: {
    fontSize: 14,
    marginLeft: 10,
    fontWeight: 'bold',
    color: '#c5c5c5',
    zIndex: -1,
  },
  dropdownArrow: {
    position: 'absolute',
    right: 0,
    bottom: 8,
    zIndex: -1,
  },
  currentLanguage: {
    position: 'absolute',
    bottom: 8,
    left: 15,
    zIndex: -1,
  },
  selectedCurrency: {
    position: 'absolute',
    left: 5,
    bottom: 15,
    zIndex: -1,
  },
  dropdownLabel: {
    color: '#a3a3a3',
    position: 'absolute',
    fontSize: 13,
    top: -21,
  },
});

export default withTranslation()(AppInfo);
