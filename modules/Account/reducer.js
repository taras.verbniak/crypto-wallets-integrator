import {
  IMPORT_EXPORT_FAIL,
  TOGGLE_ROUND_TOTAL,
  TOGGLE_ROUND_WALLETS,
  IMPORT_EXPORT_SUCCESS,
  TOGGLE_IMPORT_EXPORT_MODAL,
  CLEAR_IMPORT_EXPORT_MESSAGES,
  SET_LANGUAGE,
  SET_CURRENCY,
  SET_DISPLAY_MODE,
} from './constants';

const initialState = {
  language: null,
  currency: 'USD',
  displayMode: 0, // show all
  roundTotal: false,
  roundWallets: false,
  importExportError: null,
  importExportSuccess: null,
  importExportOpened: false,
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case TOGGLE_ROUND_TOTAL:
      return {
        ...state,
        roundTotal: !state.roundTotal,
      };

    case TOGGLE_ROUND_WALLETS:
      return {
        ...state,
        roundWallets: !state.roundWallets,
      };

    case SET_LANGUAGE:
      return {
        ...state,
        language: payload,
      };

    case SET_CURRENCY:
      return {
        ...state,
        currency: payload,
      };

    case TOGGLE_IMPORT_EXPORT_MODAL:
      return {
        ...state,
        importExportOpened: payload,
      };

    case IMPORT_EXPORT_SUCCESS:
      return {
        ...state,
        importExportSuccess: true,
      };

    case IMPORT_EXPORT_FAIL:
      return {
        ...state,
        importExportError: payload,
      };

    case CLEAR_IMPORT_EXPORT_MESSAGES:
      return {
        ...state,
        importExportError: null,
        importExportSuccess: null,
        importExportOpened: payload.closeModal ? false : true,
      };

    case SET_DISPLAY_MODE:
      return {
        ...state,
        displayMode: payload,
      };

    default:
      return state;
  }
};
