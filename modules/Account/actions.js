import {log, importFile, exportFile} from '../../helpers';
import {
  SET_LANGUAGE,
  SET_CURRENCY,
  IMPORT_BACKUP,
  SET_DISPLAY_MODE,
  IMPORT_EXPORT_FAIL,
  TOGGLE_ROUND_TOTAL,
  TOGGLE_ROUND_WALLETS,
  IMPORT_EXPORT_SUCCESS,
  TOGGLE_IMPORT_EXPORT_MODAL,
  CLEAR_IMPORT_EXPORT_MESSAGES,
} from './constants';

import {
  recalculateFiatPricesAction,
  loadBalancesAction,
} from '../WalletsList/actions';

export const toggleRoundTotalAction = () => ({type: TOGGLE_ROUND_TOTAL});

export const toggleRoundWalletsAction = () => ({type: TOGGLE_ROUND_WALLETS});

export const setLanguageAction = (payload) => ({
  type: SET_LANGUAGE,
  payload,
});

export const setCurrencyAction = (payload) => (dispatch, getState) => {
  dispatch({type: SET_CURRENCY, payload});
  recalculateFiatPricesAction(payload)(dispatch, getState);
};

export const toggleImportExportModalAction = (payload) => ({
  type: TOGGLE_IMPORT_EXPORT_MODAL,
  payload,
});

export const startImportExportAction = (isImport, importPath) => (
  dispatch,
  getState,
) => {
  try {
    const state = getState();
    const clearMessages = (timeout = 4000, payload = {}) => {
      const action = {type: CLEAR_IMPORT_EXPORT_MESSAGES, payload};
      setTimeout(() => dispatch(action), timeout);
    };

    if (isImport) {
      importFile(importPath).then(({data, error}) => {
        if (data) {
          dispatch({type: IMPORT_BACKUP, payload: data});
          dispatch({type: IMPORT_EXPORT_SUCCESS});
          loadBalancesAction()(dispatch, getState);
          clearMessages(2000, {closeModal: true, openWalletsPage: true});
        } else {
          dispatch({
            type: IMPORT_EXPORT_FAIL,
            payload: error || 'Empty file',
          });
          clearMessages();
        }
      });
    } else {
      const data = {...state};
      delete data._persist;

      exportFile(data).then((success) => {
        if (success) {
          dispatch({type: IMPORT_EXPORT_SUCCESS});
          clearMessages(2000, {closeModal: true});
        } else {
          dispatch({type: IMPORT_EXPORT_FAIL, payload: 'Internal error'});
          clearMessages();
        }
      });
    }
  } catch (error) {
    log(`ERROR in startImportExportAction: ${error}`);
    dispatch({type: IMPORT_EXPORT_FAIL, payload: 'Internal error'});
  }
};

export const setDisplayModeAction = (payload) => ({
  type: SET_DISPLAY_MODE,
  payload,
});
