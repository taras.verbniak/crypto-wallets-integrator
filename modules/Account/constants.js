import RNFS from 'react-native-fs';

export const moduleName = 'account';

export const TOGGLE_ROUND_TOTAL = `${moduleName}/TOGGLE_ROUND_TOTAL`;
export const TOGGLE_ROUND_WALLETS = `${moduleName}/TOGGLE_ROUND_WALLETS`;
export const SET_LANGUAGE = `${moduleName}/SET_LANGUAGE`;
export const SET_CURRENCY = `${moduleName}/SET_CURRENCY`;

export const IMPORT_EXPORT_FAIL = `${moduleName}/IMPORT_EXPORT_FAIL`;
export const IMPORT_EXPORT_SUCCESS = `${moduleName}/IMPORT_EXPORT_SUCCESS`;
export const TOGGLE_IMPORT_EXPORT_MODAL = `${moduleName}/TOGGLE_IMPORT_EXPORT_MODAL`;
export const CLEAR_IMPORT_EXPORT_MESSAGES = `${moduleName}/CLEAR_IMPORT_EXPORT_MESSAGES`;
export const IMPORT_BACKUP = `${moduleName}/IMPORT_BACKUP`;
export const SET_DISPLAY_MODE = `${moduleName}/SET_DISPLAY_MODE`;
export const BACKUP_FILE_NAME = 'cwi_backup.txt';
export const BACKUP_DIRECTORY = RNFS.ExternalDirectoryPath;

export const PRIVACY_URL =
  'https://drive.google.com/file/u/1/d/1RVuWJdYQWvaxpEL_4FgMT_FKbUaH7Z7i/view';

export const COUNTRIES = {
  en: {
    locale: 'en',
    title: 'English',
    icon: require('../../icons/countries/en.png'),
  },
  uk: {
    locale: 'uk',
    title: 'Українська',
    icon: require('../../icons/countries/uk.png'),
  },
  ru: {
    locale: 'ru',
    title: 'Русский',
    icon: require('../../icons/countries/ru.png'),
  },
};

export const CURRENCIES = {
  USD: {
    currencyId: 'USD',
    currencyName: 'U.S. Dollar',
    currencySymbol: '$',
  },
  EUR: {
    currencyId: 'EUR',
    currencyName: 'European euro',
    currencySymbol: '€',
  },
  JPY: {
    currencyId: 'JPY',
    currencyName: 'Japanese yen',
    currencySymbol: '¥',
  },
  GBP: {
    currencyId: 'GBP',
    currencyName: 'British pound',
    currencySymbol: '£',
  },
  CNY: {
    currencyId: 'CNY',
    currencyName: 'Chinese renminbi',
    currencySymbol: '¥',
  },
  CAD: {
    currencyId: 'CAD',
    currencyName: 'Canadian dollar',
    currencySymbol: '$',
  },
  CHF: {
    currencyId: 'CHF',
    currencyName: 'Swiss franc',
    currencySymbol: 'Fr.',
  },
  UAH: {
    currencyId: 'UAH',
    currencyName: 'Ukrainian hryvnia',
    currencySymbol: '₴',
  },
  RUB: {
    currencyId: 'RUB',
    currencyName: 'Russian ruble',
    currencySymbol: 'руб',
  },
  AUD: {
    currencyId: 'AUD',
    currencyName: 'Australian Dollar',
    currencySymbol: '$',
  },
  HRK: {
    currencyId: 'HRK',
    currencyName: 'Croatian kuna',
    currencySymbol: 'kn',
  },
  CZK: {
    currencyId: 'CZK',
    currencyName: 'Czech koruna',
    currencySymbol: 'Kč',
  },
  DKK: {
    currencyId: 'DKK',
    currencyName: 'Danish krone',
    currencySymbol: 'kr',
  },
  HKD: {
    currencyId: 'HKD',
    currencyName: 'Hong Kong dollar',
    currencySymbol: '$',
  },
  HUF: {
    currencyId: 'HUF',
    currencyName: 'Hungarian forint',
    currencySymbol: 'Ft',
  },
  INR: {
    currencyId: 'INR',
    currencyName: 'Indian rupee',
    currencySymbol: '₹',
  },
  IDR: {
    currencyId: 'IDR',
    currencyName: 'Indonesian rupiah',
    currencySymbol: 'Rp',
  },
  ILS: {
    currencyId: 'ILS',
    currencyName: 'Israeli new sheqel',
    currencySymbol: '₪',
  },
  KZT: {
    currencyId: 'KZT',
    currencyName: 'Kazakhstani tenge',
    currencySymbol: 'лв',
  },
  KRW: {
    currencyId: 'KRW',
    currencyName: 'South Korean won',
    currencySymbol: '₩',
  },
  MXN: {
    currencyId: 'MXN',
    currencyName: 'Mexican peso',
    currencySymbol: '$',
  },
  MDL: {
    currencyId: 'MDL',
    currencyName: 'Moldovan leu',
    currencySymbol: 'L',
  },
  NZD: {
    currencyId: 'NZD',
    currencyName: 'New Zealand dollar',
    currencySymbol: '$',
  },
  NOK: {
    currencyId: 'NOK',
    currencyName: 'Norwegian krone',
    currencySymbol: 'kr',
  },
  SAR: {
    currencyId: 'SAR',
    currencyName: 'Saudi riyal',
    currencySymbol: '﷼',
  },
  SGD: {
    currencyId: 'SGD',
    currencyName: 'Singapore dollar',
    currencySymbol: '$',
  },
  ZAR: {
    currencyId: 'ZAR',
    currencyName: 'South African rand',
    currencySymbol: 'R',
  },
  SEK: {
    currencyId: 'SEK',
    currencyName: 'Swedish krona',
    currencySymbol: 'kr',
  },
  EGP: {
    currencyId: 'EGP',
    currencyName: 'Egyptian pound',
    currencySymbol: '£',
  },
  BYN: {
    currencyId: 'BYN',
    currencyName: 'New Belarusian ruble',
    currencySymbol: 'p.',
  },
  AZN: {
    currencyId: 'AZN',
    currencyName: 'Azerbaijani manat',
    currencySymbol: 'ман',
  },
  RON: {
    currencyId: 'RON',
    currencyName: 'Romanian leu',
    currencySymbol: 'lei',
  },
  TRY: {
    currencyId: 'TRY',
    currencyName: 'Turkish new lira',
    currencySymbol: '₺',
  },
  GEL: {
    currencyId: 'GEL',
    currencyName: 'Georgian lari',
    currencySymbol: '₾',
  },
  BRL: {
    currencyId: 'BRL',
    currencyName: 'Brazilian real',
    currencySymbol: 'R$',
  },
  BGN: {
    currencyId: 'BGN',
    currencyName: 'Bulgarian lev',
    currencySymbol: 'лв',
  },
  PLN: {
    currencyId: 'PLN',
    currencyName: 'Polish zloty',
    currencySymbol: 'zł',
  },
  DZD: {
    currencyId: 'DZD',
    currencyName: 'Algerian dinar',
    currencySymbol: 'د.ج',
  },
  BDT: {
    currencyId: 'BDT',
    currencyName: 'Bangladeshi taka',
    currencySymbol: '৳',
  },
  AMD: {
    currencyId: 'AMD',
    currencyName: 'Armenian dram',
    currencySymbol: '֏',
  },
  IRR: {
    currencyId: 'IRR',
    currencyName: 'Iranian rial',
    currencySymbol: '﷼',
  },
  IQD: {
    currencyId: 'IQD',
    currencyName: 'Iraqi dinar',
    currencySymbol: 'ع.د',
  },
  KGS: {
    currencyId: 'KGS',
    currencyName: 'Kyrgyzstani som',
    currencySymbol: 'лв',
  },
  LBP: {
    currencyId: 'LBP',
    currencyName: 'Lebanese pound',
    currencySymbol: '£',
  },
  LYD: {
    currencyId: 'LYD',
    currencyName: 'Libyan dinar',
    currencySymbol: 'ل.د',
  },
  MYR: {
    currencyId: 'MYR',
    currencyName: 'Malaysian ringgit',
    currencySymbol: 'RM',
  },
  MAD: {
    currencyId: 'MAD',
    currencyName: 'Moroccan dirham',
    currencySymbol: 'د.م.',
  },
  VND: {
    currencyId: 'VND',
    currencyName: 'Vietnamese dong',
    currencySymbol: '₫',
  },
  THB: {
    currencyId: 'THB',
    currencyName: 'Thai baht',
    currencySymbol: '฿',
  },
  AED: {
    currencyId: 'AED',
    currencyName: 'UAE dirham',
    currencySymbol: 'فلس',
  },
  TND: {
    currencyId: 'TND',
    currencyName: 'Tunisian dinar',
    currencySymbol: 'ملّيم',
  },
  UZS: {
    currencyId: 'UZS',
    currencyName: 'Uzbekistani som',
    currencySymbol: 'лв',
  },
  TWD: {
    currencyId: 'TWD',
    currencyName: 'New Taiwan dollar',
    currencySymbol: 'NT$',
  },
  TMT: {
    currencyId: 'TMT',
    currencyName: 'Turkmenistan manat',
    currencySymbol: 'm',
  },
  GHS: {
    currencyId: 'GHS',
    currencyName: 'Ghanaian cedi',
    currencySymbol: '₵',
  },
  RSD: {
    currencyId: 'RSD',
    currencyName: 'Serbian dinar',
    currencySymbol: 'Дин.',
  },
  TJS: {
    currencyId: 'TJS',
    currencyName: 'Tajikistani somoni',
    currencySymbol: 'ЅМ',
  },
};

export const COUNTRIES_LIST = Object.values(COUNTRIES);
export const CURRENCIES_LIST = Object.values(CURRENCIES);
export const DISPLAY_MODES_LIST = [
  {value: 0, label: 'Show everything'},
  {value: 1, label: 'Hide balances'},
  {value: 2, label: 'Hide balances and wallet names'},
];
