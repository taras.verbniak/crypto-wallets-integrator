import React from 'react';
import {connect} from 'react-redux';
import {View, StyleSheet} from 'react-native';

import {getRoundTotalSelector, getRoundWalletsSelector} from './selectors';
import {getPageSelector} from '../Navbar/selectors';
import AppInfo from './components/AppInfo';
import ImportExport from './components/ImportExport';
import {
  setLanguageAction,
  setCurrencyAction,
  setDisplayModeAction,
  toggleRoundTotalAction,
  startImportExportAction,
  toggleRoundWalletsAction,
  toggleImportExportModalAction,
} from './actions';
import {
  getDisplayMode,
  getCurrencySelector,
  getImportExportError,
  getImportExportOpened,
  getImportExportSuccess,
} from './selectors';

const Account = ({
  page,
  currency,
  roundTotal,
  setLanguage,
  setCurrency,
  displayMode,
  roundWallets,
  setDisplayMode,
  toggleRoundTotal,
  startImportExport,
  importExportError,
  importExportOpened,
  toggleRoundWallets,
  importExportSuccess,
  toggleImportExportModal,
}) => {
  return (
    page === 3 && (
      <View style={styles.account}>
        <AppInfo
          currency={currency}
          roundTotal={roundTotal}
          setCurrency={setCurrency}
          displayMode={displayMode}
          setLanguage={setLanguage}
          roundWallets={roundWallets}
          setDisplayMode={setDisplayMode}
          toggleRoundTotal={toggleRoundTotal}
          toggleRoundWallets={toggleRoundWallets}
          toggleImportExportModal={toggleImportExportModal}
        />
        <ImportExport
          show={importExportOpened}
          startImportExport={startImportExport}
          importExportError={importExportError}
          importExportSuccess={importExportSuccess}
          close={() => toggleImportExportModal(false)}
        />
      </View>
    )
  );
};

const styles = StyleSheet.create({
  account: {
    flex: 1,
  },
});

const mapStateToProps = (state) => ({
  page: getPageSelector(state),
  displayMode: getDisplayMode(state),
  currency: getCurrencySelector(state),
  roundTotal: getRoundTotalSelector(state),
  roundWallets: getRoundWalletsSelector(state),
  importExportError: getImportExportError(state),
  importExportOpened: getImportExportOpened(state),
  importExportSuccess: getImportExportSuccess(state),
});

const mapDispatchToProps = {
  setCurrency: setCurrencyAction,
  setLanguage: setLanguageAction,
  setDisplayMode: setDisplayModeAction,
  toggleRoundTotal: toggleRoundTotalAction,
  startImportExport: startImportExportAction,
  toggleRoundWallets: toggleRoundWalletsAction,
  toggleImportExportModal: toggleImportExportModalAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
