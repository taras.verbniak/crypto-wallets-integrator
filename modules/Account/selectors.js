import {moduleName} from './constants';

export const getRoundTotalSelector = (state) => state[moduleName].roundTotal;
export const getCurrencySelector = (state) => state[moduleName].currency;
export const getRoundWalletsSelector = (state) =>
  state[moduleName].roundWallets;
export const getImportExportError = (state) =>
  state[moduleName].importExportError;
export const getImportExportSuccess = (state) =>
  state[moduleName].importExportSuccess;
export const getImportExportOpened = (state) =>
  state[moduleName].importExportOpened;
export const getDisplayMode = (state) => state[moduleName].displayMode;
