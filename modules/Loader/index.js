import React from 'react';
import {StyleSheet, Animated, Easing} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Loader = () => {
  const spinValue = new Animated.Value(0);

  Animated.timing(spinValue, {
    toValue: 1,
    duration: 1000,
    easing: Easing.linear,
    useNativeDriver: true,
  }).start();

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <LinearGradient
      colors={['#03060f', '#1360a8']}
      end={{x: 0, y: 0}}
      start={{x: 0, y: 1}}
      style={styles.container}>
      <Animated.Image
        style={{transform: [{rotate: spin}], width: 70, height: 70}}
        source={require('../../icons/spinner.png')}
      />
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Loader;
