import React from 'react';
import {connect} from 'react-redux';
import {View, Text, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';

import NumberFormatted from '../WalletsList/components/NumberFormatted';
import {getTotal, getIsBalancesLoading} from '../WalletsList/selectors';
import {
  getDisplayMode,
  getCurrencySelector,
  getRoundTotalSelector,
} from '../Account/selectors';

const Total = ({
  total,
  currency,
  roundTotal,
  displayMode,
  isBalancesLoading,
}) => {
  const {t} = useTranslation();
  return (
    <View style={styles.total}>
      {isBalancesLoading ? (
        <Text style={styles.loading}>{t('Loading...')}</Text>
      ) : (
        <NumberFormatted
          number={total}
          round={roundTotal}
          currency={currency}
          hiddenNumbers={!!displayMode}
          fontWeight="600"
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  total: {
    height: 170,
    paddingVertical: 30,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  loading: {
    color: '#fff',
    fontSize: 32,
    fontWeight: '600',
  },
});

const mapStateToProps = (state) => ({
  total: getTotal(state),
  displayMode: getDisplayMode(state),
  currency: getCurrencySelector(state),
  roundTotal: getRoundTotalSelector(state),
  isBalancesLoading: getIsBalancesLoading(state),
});

export default connect(mapStateToProps)(Total);
