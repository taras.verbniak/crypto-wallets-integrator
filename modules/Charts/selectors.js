import {moduleName} from './constants';

export const getHistoryBalances = (state) => state[moduleName].balances;
