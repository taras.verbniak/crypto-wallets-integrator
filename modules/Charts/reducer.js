import {LOAD_BALANCES_SUCCESS, CHANGE_WALLETS} from '../WalletsList/constants';

const initialState = {
  balances: [],
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case CHANGE_WALLETS:
    case LOAD_BALANCES_SUCCESS:
      return {
        ...state,
        balances: payload.historyBalances,
      };

    default:
      return state;
  }
};
