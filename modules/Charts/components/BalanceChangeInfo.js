import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {withTranslation} from 'react-i18next';

const BalanceChangeInfo = ({t, data, isMonthPeriod}) => {
  if (!data || data.length < 2) return null;

  const first = data[0]?.value;
  const current = data[data.length - 1]?.value;
  const previous = data[data.length - 2]?.value;

  let prevChange = Math.floor(((current - previous) / previous) * 10000) / 100;
  prevChange = isNaN(prevChange) ? 0 : prevChange;

  let firstChange = Math.floor(((current - first) / first) * 10000) / 100;
  firstChange = isNaN(firstChange) ? 0 : firstChange;

  return (
    <View style={styles.container}>
      <View style={styles.block}>
        <Text style={styles.blockText}>{t('All period')}:</Text>
        <Text
          style={{
            ...styles.blockText,
            color:
              firstChange >= 0
                ? 'rgba(0, 204, 0, 0.7)'
                : 'rgba(230, 0, 0, 0.9)',
          }}>
          {firstChange >= 0 ? `+${firstChange}` : firstChange}%
        </Text>
      </View>
      <View style={{...styles.block, marginLeft: 20}}>
        <Text style={styles.blockText}>
          {t(isMonthPeriod ? 'Day' : 'Month')}:
        </Text>
        <Text
          style={{
            ...styles.blockText,
            color:
              prevChange >= 0 ? 'rgba(0, 204, 0, 0.7)' : 'rgba(230, 0, 0, 0.9)',
          }}>
          {prevChange >= 0 ? `+${prevChange}` : prevChange}%
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  block: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  blockText: {
    color: 'rgba(197, 197, 197, 0.5)',
    marginLeft: 3,
    fontSize: 12,
    textAlign: 'center',
  },
});

export default withTranslation()(BalanceChangeInfo);
