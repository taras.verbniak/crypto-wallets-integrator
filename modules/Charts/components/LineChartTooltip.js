import React from 'react';
import {Text as SVGText, Rect} from 'react-native-svg';
import {getNumberChunks} from '../../../helpers';

const LineChartTooltip = ({
  position,
  value,
  noDecimals,
  allDecimals,
  decimalCount,
  hiddenNumbers,
  parentChartData,
}) => {
  if (position?.x === undefined || position?.y === undefined) return null;

  const formattedVal = hiddenNumbers
    ? '*****'
    : getNumberChunks(
        value?.y?.toFixed(decimalCount),
        noDecimals,
        allDecimals,
      ).join(' ');
  const adjustedWidth = Math.max(7, String(formattedVal).length) * 8;

  let lastItemsXPos = 0;
  const ITEMS_OFFSET_INDX = 6;
  const isLast5Items = value?.x > parentChartData.length - ITEMS_OFFSET_INDX;
  const isFirst6Items = value?.x < ITEMS_OFFSET_INDX;

  if (isLast5Items) {
    const itemX = value?.x - parentChartData.length || -1;

    lastItemsXPos = adjustedWidth < 60 ? 20 : 25;
    lastItemsXPos = adjustedWidth > 75 ? 30 : lastItemsXPos;
    lastItemsXPos =
      ((ITEMS_OFFSET_INDX + 1 + itemX) / ITEMS_OFFSET_INDX) * lastItemsXPos;
  } else if (isFirst6Items) {
    const itemX = value?.x || 1;

    lastItemsXPos = adjustedWidth < 60 ? -20 : -25;
    lastItemsXPos = adjustedWidth > 75 ? -30 : lastItemsXPos;
    lastItemsXPos =
      ((ITEMS_OFFSET_INDX + 1 - itemX) / ITEMS_OFFSET_INDX) * lastItemsXPos;
  }

  const shape = {
    height: 30,
    opacity: 0.8,
    width: adjustedWidth,
  };

  const styles = {
    backgroundColor: 'rgba(256,256,256, 0.5)',
  };

  const label = {
    fontSize: 11,
    fontWeight: 700,
    textAnchor: 'middle',
  };

  return (
    <React.Fragment>
      <Rect
        x={position?.x - shape.width / 2 - lastItemsXPos || 0}
        y={position?.y - shape.height / 2 - 26 || 0}
        rx={4}
        fill={styles.backgroundColor}
        height={shape.height}
        opacity={shape.opacity}
        width={shape.width}
      />
      <SVGText
        x={position?.x - lastItemsXPos || 0}
        y={position?.y - 29 || 0}
        fontSize={label.fontSize}
        fontWeight={label.fontWeight}
        fontFamily={label.fontFamily}
        fill="rgba(256,256,256, 0.75)"
        textAnchor={label.textAnchor}>
        {parentChartData[value?.x]?.label}
      </SVGText>
      <SVGText
        x={position?.x - lastItemsXPos || 0}
        y={position?.y - 16 || 0}
        fill="black"
        fontSize={label.fontSize}
        fontWeight={label.fontWeight}
        fontFamily={label.fontFamily}
        textAnchor={label.textAnchor}>
        {formattedVal}
      </SVGText>
      <SVGText
        x={position?.x + 9 || 0}
        y={position?.y - 15 || 0}
        fill={styles.backgroundColor}
        rotate={180}
        fontSize={23}
        fontWeight={label.fontWeight}
        fontFamily={label.fontFamily}
        opacity={shape.opacity}
        textAnchor={label.textAnchor}>
        ▴
      </SVGText>
    </React.Fragment>
  );
};

export default LineChartTooltip;
