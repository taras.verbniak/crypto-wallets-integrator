import React from 'react';
import {
  Area,
  Line,
  Chart,
  VerticalAxis,
  HorizontalAxis,
} from 'react-native-responsive-linechart';
import LineChartTooltip from './LineChartTooltip';
import {getNumberChunks} from '../../../helpers';

const LineChart = ({
  data: rawData,
  noDecimals,
  allDecimals,
  decimalCount,
  isMonthPeriod,
  hiddenNumbers,
}) => {
  // We need this shit just because library fails if there is same values in data, so we add tiny amount to value so it will be unique
  const data = rawData.map((item, i) => ({
    ...item,
    value: item.value + i * 0.0000000001,
  }));

  const ticks = new Array(data.length).fill().map((_, i) => i);
  const onlyValues = data.map(({value}) => value);
  const dataPrepared = onlyValues.map((y, x) => ({y, x}));
  const minValue = Math.min(...onlyValues);
  const maxValue = Math.max(...onlyValues);
  const maxY = maxValue + (maxValue - minValue) / 3;
  const yStartValue = Math.max(minValue - (maxValue - minValue) * 0.2, 0);
  const maxX = Math.max(...ticks);

  return (
    <Chart
      style={{height: 200, width: '100%'}}
      data={dataPrepared}
      padding={{left: 60, bottom: 20, right: 15, top: 20}}
      xDomain={{min: 0, max: maxX}}
      yDomain={{min: yStartValue, max: maxY}}>
      <VerticalAxis
        tickCount={hiddenNumbers ? 0 : 10}
        theme={{
          labels: {
            label: {
              color: '#c5c5c5',
              dx: -7,
            },
            formatter: (v) =>
              getNumberChunks(
                v.toFixed(Math.min(decimalCount, 4)),
                noDecimals,
                allDecimals,
              ).join(' '),
          },
          ticks: {stroke: {color: '#c5c5c5', opacity: 0.5, width: 0.5}},
          grid: {visible: false},
          axis: {visible: false},
        }}
      />
      <HorizontalAxis
        theme={{
          labels: {
            label: {
              color: '#c5c5c5',
              dy: -17,
            },
            formatter: (v) => {
              const show = isMonthPeriod ? v % 5 === 0 : true;
              return show ? data[v].label : '';
            },
          },
          ticks: {stroke: {color: '#c5c5c5', opacity: 0.5, width: 0.5}},
          grid: {visible: false},
          axis: {visible: false},
        }}
        tickValues={ticks}
      />
      <Area
        theme={{
          gradient: {
            from: {color: '#1360a8', opacity: 0.55},
            to: {color: '#608a88', opacity: 1},
          },
        }}
      />
      <Line
        tooltipComponent={
          <LineChartTooltip
            parentChartData={data}
            noDecimals={noDecimals}
            allDecimals={allDecimals}
            decimalCount={decimalCount}
            hiddenNumbers={hiddenNumbers}
          />
        }
        theme={{
          stroke: {color: 'rgba(137,173,171, 0.6)', width: 1},
          scatter: {
            default: {
              width: 4,
              height: 4,
              rx: 10,
              color: 'rgba(137,173,171, 0.6)',
            },
            selected: {color: 'white'},
          },
        }}
        hideTooltipAfter={3000}
      />
    </Chart>
  );
};

export default LineChart;
