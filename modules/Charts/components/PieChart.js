import React from 'react';
import {Text, TSpan} from 'react-native-svg';
import {PieChart} from 'react-native-svg-charts';

const PieChartComponent = ({data}) => {
  const Labels = ({slices}) => {
    return slices.map((slice, index) => (
      <Text
        key={index}
        fontSize={15}
        fill={'white'}
        stroke={'black'}
        strokeWidth={0.2}
        textAnchor={'middle'}
        alignmentBaseline={'middle'}
        x={slice.pieCentroid[0]}
        y={slice.pieCentroid[1] - 10}>
        <TSpan x={slice.pieCentroid[0] - 2}>{slice.data.symbol}</TSpan>
        <TSpan x={slice.pieCentroid[0]} dy="17">
          {`${slice.data.amount}%`}
        </TSpan>
      </Text>
    ));
  };

  return (
    <PieChart
      style={{height: 270}}
      valueAccessor={({item}) => item.amount}
      data={data}
      outerRadius={'95%'}>
      <Labels />
    </PieChart>
  );
};

export default PieChartComponent;
