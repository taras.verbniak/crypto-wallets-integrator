import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';

const Button = ({text, onPress, btnTextStyle, btnStyle}) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View style={btnStyle}>
      <Text style={btnTextStyle}>{text}</Text>
    </View>
  </TouchableWithoutFeedback>
);

export default Button;
