import React, {useState} from 'react';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import {View, ScrollView, StyleSheet, Text} from 'react-native';
import IconMaterial from 'react-native-vector-icons/dist/MaterialCommunityIcons';

import {
  getWalletShares,
  getFiatChartData,
  getCryptoChartData,
} from '../../helpers';

import BalanceChangeInfo from './components/BalanceChangeInfo';
import LineChart from './components/LineChart';
import PieChart from './components/PieChart';
import Button from './components/Button';

import {getHistoryBalances} from './selectors';
import {getPageSelector} from '../Navbar/selectors';
import {getDisplayMode, getCurrencySelector} from '../Account/selectors';
import {getRates} from '../WalletsList/selectors';

const Charts = ({t, page, balances, currency, rates, displayMode}) => {
  const YEAR_PERIOD = 'year';
  const MONTH_PERIOD = 'month';
  const BTC_NAME = 'BTC';
  const ETH_NAME = 'ETH';
  const XRP_NAME = 'XRP';
  const USDT_NAME = 'USDT';
  const USDC_NAME = 'USDC';
  const [chartPeriod, setChartPeriod] = useState(MONTH_PERIOD);
  const [selectedCrypto, setSelectedCrypto] = useState(BTC_NAME);
  const [coldInfoPosition, setColdInfoPosition] = useState(0);

  const selectedCurrency = currency || 'USD';
  const selectedPeriodStyle = 'rgba(197, 197, 197, 0.3)';
  const selectedCoinBtnStyle = 'rgba(197, 197, 197, 0.3)';

  const dayDuration = 1000 * 60 * 60 * 24;
  const timeNow = new Date().getTime();
  const monthTimeBehind = timeNow - dayDuration * 30;
  const yearTimeBehind = timeNow - dayDuration * 365;
  const isMonthPeriod = chartPeriod === MONTH_PERIOD;
  const timePeriod = isMonthPeriod ? monthTimeBehind : yearTimeBehind;
  const periodData = balances
    ? balances?.filter(({timestamp}) => timestamp > timePeriod)
    : [];
  const fiatRate = rates && rates[currency] ? rates[currency] : 1;
  const lastPeriodData = periodData[periodData.length - 1]?.balances;

  const fiatChartData = getFiatChartData(periodData, isMonthPeriod, fiatRate);
  const cryptoChartData = getCryptoChartData(
    periodData,
    isMonthPeriod,
    selectedCrypto,
  );
  const walletsShares = getWalletShares(lastPeriodData);
  const availableCryptoCoins = [
    BTC_NAME,
    ETH_NAME,
    XRP_NAME,
    USDT_NAME,
    USDC_NAME,
  ].filter((c) => !!(lastPeriodData || {})[c]);

  // workaroung for -50% transform, since react doesn't support percentage in transform...
  const saveColdInfoPos = (e) => {
    const width = e?.nativeEvent?.layout?.width || 0;
    const position = width / 2 || 0;
    setColdInfoPosition(position);
  };

  return (
    page === 2 && (
      <ScrollView style={styles.container}>
        <View style={styles.periodBtnView}>
          <Button
            text={t('Month')}
            btnStyle={{
              ...styles.periodBtn,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              backgroundColor: isMonthPeriod ? selectedPeriodStyle : null,
            }}
            btnTextStyle={styles.periodBtnText}
            onPress={() => setChartPeriod(MONTH_PERIOD)}
          />
          <Button
            text={t('Year')}
            btnStyle={{
              ...styles.periodBtn,
              borderTopRightRadius: 10,
              borderBottomRightRadius: 10,
              backgroundColor: isMonthPeriod ? null : selectedPeriodStyle,
            }}
            btnTextStyle={styles.periodBtnText}
            onPress={() => setChartPeriod(YEAR_PERIOD)}
          />
        </View>
        {fiatChartData?.length > 1 && (
          <>
            <Text style={styles.blockTitle}>
              {`${t('Total balance')} ${selectedCurrency}`}
            </Text>
            <BalanceChangeInfo
              data={fiatChartData}
              isMonthPeriod={isMonthPeriod}
            />
            <LineChart
              noDecimals
              data={fiatChartData}
              isMonthPeriod={isMonthPeriod}
              hiddenNumbers={!!displayMode}
            />
          </>
        )}
        <Text style={styles.blockTitle}>{t('Crypto balances')}</Text>
        <View
          style={{
            ...styles.periodBtnView,
            justifyContent: 'space-evenly',
            marginBottom: 10,
          }}>
          {availableCryptoCoins.map((coin) => (
            <Button
              text={coin}
              btnStyle={{
                ...styles.coindBtn,
                backgroundColor:
                  selectedCrypto === coin ? selectedCoinBtnStyle : null,
              }}
              btnTextStyle={styles.coindBtnText}
              onPress={() => setSelectedCrypto(coin)}
            />
          ))}
        </View>
        <BalanceChangeInfo
          data={cryptoChartData}
          isMonthPeriod={isMonthPeriod}
        />
        {cryptoChartData?.length > 1 && (
          <LineChart
            allDecimals
            data={cryptoChartData}
            isMonthPeriod={isMonthPeriod}
            hiddenNumbers={!!displayMode}
            noDecimals={selectedCrypto === XRP_NAME}
            decimalCount={
              selectedCrypto === BTC_NAME
                ? 8
                : selectedCrypto === ETH_NAME
                ? 3
                : 2
            }
          />
        )}
        <View style={{marginBottom: 100}}>
          <Text style={styles.blockTitle}>{t('Wallet shares')}</Text>
          <View
            style={{
              ...styles.coldInfo,
              transform: [{translateX: -coldInfoPosition}],
            }}
            onLayout={saveColdInfoPos}>
            <IconMaterial name="sack" size={30} color="rgba(77,107,83, 0.8)" />
            <Text style={styles.coldInfoText}>
              {`${periodData[periodData?.length - 1]?.coldBalanceShare}%`}
            </Text>
          </View>
          <PieChart data={walletsShares} />
        </View>
      </ScrollView>
    )
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 55,
  },
  blockTitle: {
    color: '#fff',
    fontSize: 20,
    width: '100%',
    paddingTop: 50,
    paddingBottom: 10,
    textAlign: 'center',
  },
  periodBtnView: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  periodBtn: {
    borderColor: 'rgba(197, 197, 197, 0.5)',
    borderWidth: 1,
    width: 90,
    paddingVertical: 9,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  coindBtn: {
    borderColor: 'rgba(197, 197, 197, 0.5)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    width: 55,
    borderWidth: 1,
    lineHeight: 2,
    paddingVertical: 3,
  },
  coindBtnText: {
    color: '#c5c5c5',
    fontSize: 14,
    marginLeft: 3,
    lineHeight: 18,
  },
  periodBtnText: {
    color: '#c5c5c5',
    fontSize: 16,
    marginLeft: 3,
    lineHeight: 18,
  },
  coldInfo: {
    position: 'absolute',
    top: 205,
    left: '50%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  coldInfoText: {
    color: 'rgba(77,107,83, 0.9)',
    fontSize: 16,
  },
});

const mapStateToProps = (state) => ({
  rates: getRates(state),
  page: getPageSelector(state),
  displayMode: getDisplayMode(state),
  balances: getHistoryBalances(state),
  currency: getCurrencySelector(state),
});

const chartsWithTranslation = withTranslation()(Charts);

export default connect(mapStateToProps)(chartsWithTranslation);
