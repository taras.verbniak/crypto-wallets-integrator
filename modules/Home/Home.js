import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {AppState, View, StyleSheet} from 'react-native';

import Total from '../Total/Total';
import WalletsList from '../WalletsList/WalletsList';

import {getPageSelector} from '../Navbar/selectors';
import {loadBalancesAction} from '../WalletsList/actions';

const Home = ({page, loadBalances}) => {
  const updater = (type) => type === 'active' && loadBalances();

  useEffect(() => {
    AppState.addEventListener('change', updater);
    return () => {
      AppState.removeEventListener('change', updater);
    };
  }, []);

  return (
    page === 1 && (
      <View style={styles.home}>
        <Total />
        <WalletsList />
      </View>
    )
  );
};

const styles = StyleSheet.create({
  home: {
    flex: 1,
  },
});

const mapStateToProps = (state) => ({
  page: getPageSelector(state),
});

const mapDispatchToProps = {
  loadBalances: loadBalancesAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
