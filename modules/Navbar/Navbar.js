import React from 'react';
import {connect} from 'react-redux';
import {View, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import IconFA from 'react-native-vector-icons/dist/FontAwesome5';

import {setPageAction} from './actions';
import {getPageSelector} from './selectors';

const Navbar = ({page, setPage}) => (
  <View style={styles.navbar}>
    <TouchableWithoutFeedback onPress={() => setPage(1)}>
      <View style={styles.btnLeftContainer}>
        <Icon
          name="credit-card"
          size={45}
          color={page === 1 ? '#608a88' : 'rgba(255,255,168, 0.2)'}
        />
      </View>
    </TouchableWithoutFeedback>
    <TouchableWithoutFeedback onPress={() => setPage(2)}>
      <View style={styles.btnRightContainer}>
        <Icon
          name="chart"
          size={45}
          color={page === 2 ? '#608a88' : 'rgba(255,255,168, 0.2)'}
        />
      </View>
    </TouchableWithoutFeedback>
    <TouchableWithoutFeedback onPress={() => setPage(3)}>
      <View style={styles.btnRightContainer}>
        <IconFA
          name="user"
          size={24}
          color={page === 3 ? '#608a88' : 'rgba(255,255,168, 0.15)'}
        />
      </View>
    </TouchableWithoutFeedback>
  </View>
);

const styles = StyleSheet.create({
  navbar: {
    height: 50,
    paddingRight: 50,
    paddingLeft: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#0B1D37',
    paddingRight: 33,
    paddingLeft: 45,
  },
  btnLeftContainer: {
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  btnRightContainer: {
    paddingVertical: 10,
    paddingHorizontal: 17,
  },
});

const mapStateToProps = (state) => ({
  page: getPageSelector(state),
});

const mapDispatchToProps = {
  setPage: setPageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
