import {SET_PAGE} from './constants';
import {CLEAR_IMPORT_EXPORT_MESSAGES} from '../Account/constants';

const initialState = {page: 1};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case SET_PAGE:
      return {
        ...state,
        page: payload,
      };

    case CLEAR_IMPORT_EXPORT_MESSAGES:
      return {
        ...state,
        page: payload.openWalletsPage ? 1 : state.page,
      };

    default:
      return state;
  }
};
