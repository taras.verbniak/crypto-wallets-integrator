import {moduleName} from './constants';

export const getPageSelector = (state) => state[moduleName].page;
