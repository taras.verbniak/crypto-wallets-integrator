import {SET_PAGE} from './constants';

export const setPageAction = (payload) => ({type: SET_PAGE, payload});
