import {Linking} from 'react-native';
import {log} from './logger';

export const openLink = async (url) => {
  try {
    const supported = Linking.canOpenURL(url);

    if (supported) {
      Linking.openURL(url);
    } else {
      log(`WARNING in openLink: can't open url`);
    }
  } catch (error) {
    log(`ERROR in openLink: ${error}`);
  }
};
