import {log} from './logger';

export const isNumber = (n) => !isNaN(Number(n));

export const round = (n, rounder = 100) => {
  let res = n;

  try {
    const number = parseFloat(n);
    res = Math.floor(number * rounder) / rounder;
  } catch (error) {
    log(`ERROR in round: ${error}`);
  }

  return res;
};

const splitNumber = (text) => {
  let res = [text];

  try {
    if (text.length > 3) {
      res = [];
      let counter = 1;
      let prevIndex = text.length;
      for (let i = text.length - 2; i > -1; i--) {
        counter += 1;
        if (counter % 3 === 0 || i === 0) {
          res.unshift(text.slice(i, prevIndex));
          prevIndex = i;
        }
      }
    }
  } catch (error) {
    log(`ERROR in splitNumber: ${error}`);
  }

  return res;
};

export const getNumberChunks = (n, noDecimal, allDecimals) => {
  let res = [n];

  try {
    if (n) {
      let [part0, part1 = ''] = `${n}`.split('.');

      const integerChunks = splitNumber(part0);

      if (noDecimal) {
        res = integerChunks;
      } else {
        part1 = !part1.length ? `${part1}00` : part1;
        part1 = part1.length === 1 ? `${part1}0` : part1;

        if (allDecimals) {
          part1 = part1.slice(0, 8);
        } else {
          part1 = part1.slice(0, 2);
        }

        let lastChunk = integerChunks[integerChunks.length - 1];
        lastChunk = `${lastChunk}.${part1}`;

        integerChunks[integerChunks.length - 1] = lastChunk;

        res = integerChunks;
      }
    }
  } catch (error) {
    log(`ERROR in getNumberChunks: ${error}`);
  }

  return res;
};
