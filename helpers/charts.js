import {log} from './logger';
import {COINS} from '../modules/WalletsList/constants';

const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

const getCurrentMonthStartTime = () => {
  let result = null;

  try {
    const timeNow = new Date();
    const currentYear = timeNow.getFullYear();
    const currentMonth = months[timeNow.getMonth()];

    result = new Date(`01 ${currentMonth} ${currentYear}`).getTime();
  } catch (error) {
    log(`ERROR in getCurrentMonthStartTime: ${error}`);
  }

  return result;
};

export const getFiatChartData = (balances, isMonthPeriod, fiatRate) => {
  let data = [];

  try {
    if (balances) {
      if (isMonthPeriod) {
        data = balances.map((item) => {
          const value = Object.values(item.balances).reduce(
            (acc, {usdtBalance}) => acc + usdtBalance,
            0,
          );
          const label = item.date.slice(0, 5);
          return {label, value: Math.round(value * fiatRate * 100) / 100};
        });
      } else {
        let monthStart = getCurrentMonthStartTime();
        let monthEnd = new Date().getTime() + 1000; // time now and 1s to be sure
        const dayDuration = 1000 * 60 * 60 * 24;
        const monthDuration = dayDuration * 30;

        // 12 months
        for (let i = 1; i <= 12; i += 1) {
          const balanceSlice = balances?.filter(
            ({timestamp}) => timestamp > monthStart && timestamp < monthEnd,
          );
          const balanceTotal = balanceSlice.reduce((acc, item) => {
            const dayTotal = Object.values(item.balances).reduce(
              (acc, {usdtBalance}) => acc + usdtBalance,
              0,
            );
            return acc + dayTotal;
          }, 0);
          const monthBalance = balanceTotal / balanceSlice.length;

          if (monthBalance) {
            const current = {
              label: months[new Date(monthStart + dayDuration * 15).getMonth()],
              value: Math.round(monthBalance * fiatRate * 100) / 100,
            };

            data = [current, ...data];
          }

          monthEnd = monthStart;
          monthStart = monthStart - monthDuration;
        }
      }
    }
  } catch (error) {
    log(`ERROR in getWalletShares: ${error}`);
  }

  return data;
};

export const getCryptoChartData = (balances, isMonthPeriod, symbol) => {
  let data = [];

  try {
    if (balances) {
      if (isMonthPeriod) {
        data = balances.map((item) => {
          const value = item?.balances[symbol]?.balance;
          const label = item?.date?.slice(0, 5);
          return {label, value: Math.round(value * 100000000) / 100000000};
        });
      } else {
        let monthStart = getCurrentMonthStartTime();
        let monthEnd = new Date().getTime() + 1000; // time now and 1s to be sure
        const dayDuration = 1000 * 60 * 60 * 24;
        const monthDuration = dayDuration * 30;

        // 12 months
        for (let i = 1; i <= 12; i += 1) {
          const balanceSlice = balances?.filter(
            ({timestamp}) => timestamp > monthStart && timestamp < monthEnd,
          );
          const balanceTotal = balanceSlice.reduce((acc, item) => {
            const dayTotal = item?.balances[symbol]?.balance;
            return acc + dayTotal;
          }, 0);
          const monthBalance = balanceTotal / balanceSlice.length;

          if (monthBalance) {
            const current = {
              label: months[new Date(monthStart + dayDuration * 15).getMonth()],
              value: Math.round(monthBalance * 100000000) / 100000000,
            };

            data = [current, ...data];
          }

          monthEnd = monthStart;
          monthStart = monthStart - monthDuration;
        }
      }

      data = data.filter(({value}) => value);
    }
  } catch (error) {
    log(`ERROR in getWalletShares: ${error}`);
  }

  return data;
};

export const getWalletShares = (balances) => {
  const colors = {
    BTC: 'rgba(52,55,74,0.9)',
    ETH: 'rgba(68,72,97,0.9)',
    XRP: 'rgba(84,90,121,0.9)',
    USDT: 'rgba(179,183,202,0.4)',
    USDC: 'rgba(216,212,212,0.5)',
  };

  let shares = [];

  try {
    if (balances) {
      const total = Object.values(balances).reduce(
        (acc, item) => acc + item?.usdtBalance,
        0,
      );

      shares = Object.entries(balances)
        .sort(([a], [b]) => COINS[a].position - COINS[b].position)
        .map((item, key) => ({
          key,
          symbol: item[0],
          svg: {fill: colors[item[0]]},
          amount:
            Math.round((balances[item[0]]?.usdtBalance / total) * 1000) / 10,
        }));
    }
  } catch (error) {
    log(`ERROR in getWalletShares: ${error}`);
  }

  return shares;
};
