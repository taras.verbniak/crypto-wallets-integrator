import {dateToOurFormat} from './time';
import {log} from './logger';

export const calcBalancesForHistory = (savedBalances, wallets) => {
  let result = savedBalances;

  try {
    const timestamp = new Date().getTime();
    const dateNow = dateToOurFormat(timestamp);
    const lastSavedBalance = savedBalances[savedBalances.length - 1];
    const balances = wallets?.reduce((acc, item) => {
      const {symbol, balance, usdtBalance} = item;
      if (!acc[symbol]) {
        acc[symbol] = {balance, usdtBalance};
      } else {
        acc[symbol].balance += balance;
        acc[symbol].usdtBalance += usdtBalance;
      }
      return acc;
    }, {});

    const totalBalance = wallets?.reduce(
      (acc, {usdtBalance}) => acc + usdtBalance,
      0,
    );
    const totalColdBalance = wallets?.reduce(
      (acc, {isColdWallet, usdtBalance}) =>
        isColdWallet ? acc + usdtBalance : acc,
      0,
    );
    const coldBalanceShare =
      Math.floor((totalColdBalance / totalBalance) * 1000) / 10 || 0;

    const currentBalance = {
      date: dateNow,
      timestamp,
      balances,
      coldBalanceShare,
    };

    if (!lastSavedBalance) {
      result = [currentBalance];
    } else if (lastSavedBalance.date === dateNow) {
      // the same day - just update
      result = [...savedBalances.slice(0, -1), currentBalance];
    } else {
      // new day
      result = [...savedBalances, currentBalance];
    }
  } catch (error) {
    log(`ERROR in calcBalancesForHistory: ${error}`);
  }

  return result;
};
