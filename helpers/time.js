import {log} from './logger';

export const olderThanOneDay = (timeString) => {
  let older = true;

  try {
    const convertedTime = timeString.split('.').reverse().join('-');
    const prevTime = new Date(convertedTime).getTime();
    const timeNow = new Date().getTime();
    const oneDay = 1000 * 60 * 60 * 24;

    older = timeNow > prevTime + oneDay;
  } catch (error) {
    log(`ERROR in olderThanOneDay: ${error}`);
  }

  return older;
};

export const dateToOurFormat = (timeString) => {
  let result = timeString;

  try {
    const time = new Date(timeString);
    let day = time.getDate();
    let month = time.getMonth() + 1;
    const year = time.getFullYear();

    day = day < 10 ? `0${day}` : day;
    month = month < 10 ? `0${month}` : month;

    result = `${day}/${month}/${year}`;
  } catch (error) {
    log(`ERROR in dateToOurFormat: ${error}`);
  }

  return result;
};
