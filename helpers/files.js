import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';

import {log} from './logger';
import {BACKUP_FILE_NAME, BACKUP_DIRECTORY} from '../modules/Account/constants';

export const pickFile = () =>
  new Promise(async (done) => {
    try {
      const fileType = DocumentPicker.types.allFiles;
      const {uri} = await DocumentPicker.pick({
        type: [fileType],
      });

      const split = unescape(uri).split('/');
      const name = split.pop();

      done(name);
    } catch (error) {
      if (DocumentPicker.isCancel(error)) {
        done(null, 'Cancelled');
      } else {
        log(`ERROR in importFile: ${error}`);
        done(null, 'Internal error');
      }
    }
  });

export const importFile = (name) =>
  new Promise(async (done) => {
    try {
      const fileName = name || BACKUP_FILE_NAME;
      const path = `${BACKUP_DIRECTORY}/${fileName}`;
      RNFS.readFile(path, 'utf8')
        .then((stringData) => {
          try {
            const data = JSON.parse(stringData);

            if (data.account && data.charts && data.navbar && data.wallets) {
              done({data});
            }
          } catch (error) {
            done({
              error:
                'Document with wrong structure. Please try to upload another one or add wallets manually',
            });
          }

          done({
            error:
              'Document with wrong structure. Please try to upload another one or add wallets manually',
          });
        })
        .catch(() => {
          done({error: 'Document upload error. Only Downloads folder allowed'});
        });
    } catch (error) {
      log(`ERROR in importFile: ${error}`);
      done({error: 'Internal error'});
    }
  });

export const deleteFile = (path) =>
  new Promise((done) => {
    try {
      RNFS.unlink(path)
        .then(() => {})
        .catch(() => {})
        .then(() =>
          RNFS.scanFile(path)
            .then(() => {})
            .catch(() => {})
            .then(() => done()),
        );
    } catch (error) {
      done();
    }
  });

export const exportFile = (data) =>
  new Promise((done) => {
    try {
      const path = `${BACKUP_DIRECTORY}/${BACKUP_FILE_NAME}`;
      const dataJSON = JSON.stringify(data);

      deleteFile(path).then(() => {
        RNFS.writeFile(path, dataJSON)
          .then(() => done(true))
          .catch((err) => {
            log(`ERROR in exportFile RNFS: ${err}`);
            done(false);
          });
      });
    } catch (error) {
      log(`ERROR in exportFile: ${error}`);
      done(false);
    }
  });
