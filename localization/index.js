import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {getLocales} from 'react-native-localize';
import {log} from '../helpers';

import en from './en.json';
import uk from './uk.json';
import ru from './ru.json';

const resources = {en, uk, ru};

const initLocalization = (store) => {
  try {
    const state = store?.getState();
    const savedLanguage = state?.account?.language;
    const deviceLocales = getLocales() || [];
    const deviceLanguage = deviceLocales[0]?.languageCode;

    const lng = savedLanguage || deviceLanguage || 'en';

    i18n.use(initReactI18next).init({
      lng,
      resources,
      fallbackLng: 'en',
      keySeparator: false,
      interpolation: {escapeValue: false},
    });
  } catch (error) {
    log(`ERROR in initLocalization: ${error}`);
  }
};

export {initLocalization};
